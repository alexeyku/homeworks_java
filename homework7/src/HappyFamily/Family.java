package HappyFamily;

import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] chiildren;
    private Pet pet;


    static {
        System.out.println("Family initialised");
    }

    {
        System.out.println("New Family`s object is creating");
    }


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.chiildren = null;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }


//    methods


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father) &&
                Arrays.equals(chiildren, family.chiildren);
    }

    @Override
    public int hashCode() {
        int result = this.mother.hashCode() + this.father.hashCode() * 31;
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                " \n mother= " + this.mother +
                ",\n father= " + this.father +
                ",\n chiildren= " + Arrays.toString(this.chiildren) +
                ",\n pet=" + this.pet +
                '}';
    }

    public void addChild(Human child){
        child.setFamily(this);
        if(this.chiildren != null && Arrays.asList(this.chiildren).indexOf(child) == -1){
            this.chiildren = Arrays.copyOf(this.chiildren,this.chiildren.length+1);
            this.chiildren[this.chiildren.length-1] = child;
        }
        else if(this.chiildren == null){
            this.chiildren = new Human[1];
            this.chiildren[0] = child;
        }
    }

    public void deleteChild(int index){
        if(isNotEmpty() && index <= this.chiildren.length-1){
            Human[] newArr = new Human[this.chiildren.length-1];
            int j = 0;
            for (int i = 0; i < this.chiildren.length; i++) {
                if(i == index) continue;
                newArr[j] = this.chiildren[i];
                j++;
            }
            this.chiildren = newArr;
        }
    }

    public void deleteChild(Human child){
        if(isNotEmpty() && Arrays.asList(this.chiildren).indexOf(child) != -1){
            Human[] newArr = new Human[this.chiildren.length-1];
            int j = 0;
            for (int i = 0; i < this.chiildren.length; i++) {
                if(child.hashCode() == this.chiildren[i].hashCode()){
                    if(child.equals(this.chiildren[i])) continue;
                }
                else{
                    newArr[j] = this.chiildren[i];
                    j++;
                }

            }
            this.chiildren = newArr;
        }
    }

    public boolean isNotEmpty(){
        if(this.chiildren != null && this.chiildren.length != 0) return true;
        else return false;
    }


    public int countFamily(){
        int result = 2;
        if(this.chiildren == null) return result;
        result += this.chiildren.length;
        return result;
    }


    //    getters/setters
    public Human getMother() {
        return this.mother;
    }

    public Human getFather() {
        return this.father;
    }

    public Human[] getChiildren() {
        return this.chiildren;
    }

    public Pet getPet() {
        return this.pet;
    }

    public void setPet(Pet pet) {
        this.pet = pet;
    }


    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChiildren(Human[] chiildren) {
        this.chiildren = chiildren;
    }
}
