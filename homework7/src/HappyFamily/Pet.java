package HappyFamily;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Pet initialised");
    }

    {
        System.out.println("New Pet`s object is creating");
    }

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return age == pet.age &&
                Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return this.species.hashCode() + this.nickname.hashCode() + this.age * 31;
    }

    //    methods
    public String eat() {
        return  "Я кушаю!";
    }

    public String respond() {
        return "Привет, хозяин. Я - " + this.nickname + ". Я соскучился!";
    }

    public String foul() {
        return "Нужно хорошо замести следы...";
    }

    public String toString() {
        String output = this.species + "{\n\tnickname = '" + this.nickname + "', age = " + this.age + ",trickLevel = " + this.trickLevel + ", habits = " + Arrays.toString(this.habits) + "\n\t}";
        return output;
    }


    //    getters
    public String getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public String[] getHabits() {
        return this.habits;
    }

    public void setSpecies(String species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
