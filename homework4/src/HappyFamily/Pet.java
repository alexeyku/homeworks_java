package HappyFamily;

import java.util.Arrays;

public class Pet {
    private String species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    public Pet(String species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    //    methods
    public String eat() {
        return  "Я кушаю!";
    }

    public String respond() {
        return "Привет, хозяин. Я - " + this.nickname + ". Я соскучился!";
    }

    public String foul() {
        return "Нужно хорошо замести следы...";
    }

    public String toString() {
        String output = this.species + "{\n\tnickname = '" + this.nickname + "', age = " + this.age + ",trickLevel = " + this.trickLevel + ", habits = " + Arrays.toString(this.habits) + "\n\t}";
        return output;
    }


    //    getters
    public String getSpecies() {
        return this.species;
    }

    public String getNickname() {
        return this.nickname;
    }

    public int getAge() {
        return this.age;
    }

    public int getTrickLevel() {
        return this.trickLevel;
    }

    public String getHabits() {
        return Arrays.toString(this.habits);
    }



}
