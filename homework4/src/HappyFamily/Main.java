package HappyFamily;


public class Main {
    public static void main(String[] args) {
        String[] arr = {"eat, drink, sleep"};
        String[][] motherScedule = {
                {"Saturday","relax with family"},
                {"Sunday","walk with child"},
        };
        String[][] fatherScedule = {
                {"Saturday","relax with family"},
                {"Sunday","visit parents"}
        };

        String[][] childScedule = {
                {"Saturday","relax with family"},
                {"Sunday","walk with mother"}
        };

        Pet dog = new Pet("dog","Bobik",7,51,arr);

        Human mother = new Human("Jane", "Karleone",1977,90,dog,new Human("Judi","Smith"),new Human("Jake","Karleone"),motherScedule);

        Human father = new Human("Vito", "Karleone",1974,95,dog,new Human("Juli","Karleone"),new Human("Alex","Karleone"),fatherScedule);

        Human child = new Human("Michael", "Karleone",2007,85,dog,mother,father,childScedule);

        System.out.println(child.greetPet());
        System.out.println(child.describePet());
        System.out.println(child.toString());

        child.feedPet(false);

    }

}
