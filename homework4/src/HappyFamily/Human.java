package HappyFamily;

import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Pet pet;
    private Human mother;
    private Human father;
    private String[][] scedule;


    public Human(String name, String surname){
        this.name = name;
        this.surname = surname;
    }

    public Human(String name, String surname, int year , int iq, Pet pet, Human mother, Human father, String[][] scedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.pet = pet;
        this.mother = mother;
        this.father = father;
        this.scedule = scedule;
    }

//    methods
    public String toString(){
        String output = this.getClass().getSimpleName() + "{\n name = " + this.name + ",\n surname = " + this.surname + ",\n year = " + this.year + ",\n iq = " + this.iq + ",\n mother = " + this.mother.name + " " + this.mother.surname + ",\n father = " + this.father.name + " " + this.father.surname +  ",\n pet = " + this.pet.toString() + "\n}";
        return output;
    }

    public String greetPet(){
        return "Привет, " + this.pet.getNickname();
    }

    public String describePet(){
        String trick = "";
        if(this.pet.getTrickLevel() > 50 ){
            trick = "очень хитрый";
        }
        else{
            trick = "почти не хитрый";
        }
        String output = "У меня есть " + this.pet.getSpecies() + " , ему " + this.pet.getAge() + " лет, он " + trick;
        return output;
    }

    public boolean feedPet(boolean flag){
        String output = "";
        boolean result = false;
        if(flag == true){
            output = "Хм... покормлю ка я " + this.pet.getNickname();
            result = true;
        }
        else{
            Random ran = new Random();
            int random = ran.nextInt(101);
            if(this.pet.getTrickLevel() > random){
                output = "Хм... покормлю ка я " + this.pet.getNickname();
                result = true;
            }
            else{
                output = "Думаю, " + this.pet.getNickname() + " не голоден.";
            }
        }
        System.out.println(output);
        return result;
    }
}
