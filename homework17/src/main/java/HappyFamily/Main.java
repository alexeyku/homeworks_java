package HappyFamily;

import HappyFamily.Controller.FamilyController;
import HappyFamily.Enums.DayOfWeek;
import HappyFamily.People.*;
import HappyFamily.DAO.*;
import HappyFamily.Service.FamilyService;

import java.util.*;

public class Main {
    public final static Scanner in = new Scanner(System.in);
    public final static String validateNumber = "([0-9])+";
    public final static String valdiateString = "\\D+";
    public static void main(String[] args) {
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

        Map<String, String> motherScedule = new HashMap<String, String>();
        motherScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(), "walk with child");

        Map<String, String> fatherScedule = new HashMap<String, String>();
        fatherScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(), "visit parents");

        Map<String, String> childScedule = new HashMap<String, String>();
        childScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(), "walk with mother");

        Woman mother = new Woman("Mary", "Winchester", "10/10/1974", 90, motherScedule);
        Man father = new Man("John", "Winchester", "22/05/1985", 95, fatherScedule);

        Woman mother2 = new Woman("Jane", "Smith", "14/07/1980", 90, motherScedule);
        Man father2 = new Man("Bred", "Smith", "27/09/1982", 95, fatherScedule);

        Woman mother3 = new Woman("Kate", "Sur", "14/07/1980", 90, motherScedule);
        Man father3 = new Man("Bred", "Sur", "27/09/1982", 95, fatherScedule);

        Woman mother4 = new Woman("Suzan", "Valera", "10/01/1985", 90, motherScedule);
        Man father4 = new Man("Max", "Valera", "29/07/1975", 95, fatherScedule);

        Human child = new Man("Valera", "Valerich", "16/02/1998", 95, childScedule);
        Human child2 = new Woman("Valerian", "Surnamich", "10/08/1999", 95, childScedule);
        Human child3 = new Man("Alex", "Alexich", "11/07/2002", 95, childScedule);
        Human child4 = new Man("Deny", "Denich", "08/07/2008", 95, childScedule);

        FamilyDao dataBase = new CollectionFamilyDao();
        FamilyService service = new FamilyService(dataBase);
        FamilyController controller = new FamilyController(service);


        loop:
        while (true){
            int number = 0;
            String index;
            generateCommands();
            String answer = in.nextLine();
            switch (answer){
                case "1" :
                    controller.createNewFamily(mother,father);
                    controller.createNewFamily(mother2,father2);
                    continue loop;
                case "2":
                    controller.displayAllFamilies();
                    continue loop;
                case "3":
                    number = enteringNumber(false,true);
                    controller.getFamiliesBiggerThan(number);
                    continue loop;
                case "4":
                    number = enteringNumber(false,true);
                    controller.getFamiliesLessThan(number);
                    continue loop;
                case "5":
                    number = enteringNumber(false,true);
                    System.out.println(controller.countFamiliesWithMemberNumber(number));
                    continue loop;
                case "6":
                    controller.createNewFamily(generateHuman(true,false),generateHuman(false,false));
                    continue loop;

                case "7":
                    number = enteringNumber(true,true);
                    controller.deleteFamilyByIndex(number);
                    continue loop;

                case "8":
                    inner:
                    while (true){
                        generateForEditingCase();
                        String answerForEditing = in.nextLine();
                        switch (answerForEditing){
                            case "1":
                                number = enteringNumber(true,true);
                                if(controller.getFamilyById(number-1) == null) {
                                    System.out.println("No family with following id");
                                    continue inner;
                                }

                                String boyName = "",girlName="";

                                boyName = enteringString(true,false);
                                girlName = enteringString(true,true);

                                controller.bornChild(controller.getFamilyById(number-1),boyName,girlName);
                                continue loop;

                            case "2":
                                 number = enteringNumber(true,true);

                                if(controller.getFamilyById(number-1) == null) {
                                    System.out.println("No family with following id");
                                    continue inner;
                                }

                                System.out.println("Enter gender(M or man, W or woman)");
                                String gender = in.nextLine();
                                while (!gender.matches("(?i:m|w|man|woman)")){
                                    System.out.println("Invalid input. Enter M or man, W or woman");
                                    gender = in.nextLine();
                                }

                                boolean gen = gender.trim().matches("(?i:m|man)") ? false : true;
                                controller.adoptChild(controller.getFamilyById(number-1),generateHuman(gen,true));
                                continue loop;
                            case "3":
                                continue loop;
                            default :
                                System.out.println("Invalid input. Enter once more");
                                continue inner;
                        }
                    }


                case "9":
                    System.out.println("Enter age");
                    String age = in.nextLine();
                    while(!age.matches(validateNumber)){
                        System.out.println("Invalid age. Enter once more");
                        age = in.nextLine();
                    }
                    controller.deleteAllChildrenOlderThen(Integer.parseInt(age));
                    continue loop;

                case "exit" :
                    break loop;

                default:
                    System.out.println("Invalid input. Enter once more");
                    continue loop;
            }
        }
    }

    public static String enteringString(boolean nameFlag, boolean gender){
//        gender = true - girl
        if(nameFlag) System.out.println("Enter " + (gender ? "girl's" : "boy's") + " name");
        String testStr = in.nextLine();
        while (!testStr.matches(valdiateString)){
            System.out.println("Invalid input. Enter name once more");
            testStr = in.nextLine();
        }
        return testStr;
    }

    public static int enteringNumber(boolean index,boolean messageFlag){
        if(messageFlag) System.out.println(!index ? "Enter a  number" : "Enter an id of family(starting from 1)");
        String testStr = in.nextLine();
        while (!testStr.matches(validateNumber)){
            System.out.println("Invalid input. You must enter a number(0-9).Enter once more");
            testStr = in.nextLine();
        }
        return Integer.parseInt(testStr);
    }

    public static String enteringBirthdayData(boolean year, boolean month){
        String testStr = in.nextLine();
        while (!testStr.matches(year ? "([0-9]){4}" : month ? "(1[0-2]|0[1-9])$" : "((1)[0-9]|(2)[0-9]|0[1-9]|3[0-1])$")){
            System.out.println("Invalid date. Enter once more");
            testStr = in.nextLine();
        }
        return testStr;
    }

    public static Human generateHuman(boolean gender,boolean child){
//        gender = true - Woman
        String name = "" , surname = "",year = "", month="",day = "";
        int iq=0;

        System.out.println(!child ? gender ? "Enter mother's name" :  "Enter father's name" : "Enter name");
         name = enteringString(false,false);

        System.out.println(!child ? gender ? "Enter mother's surname" :  "Enter father's surname" : "Enter surname");
         surname = enteringString(false,false);

        System.out.println(!child ? gender ? "Enter mother's year(yyyy)" :  "Enter father's year(yyyy)" : "Enter year(yyyy)");
         year = enteringBirthdayData(true,false);

        System.out.println(!child ? gender ? "Enter mother's month(mm)" :  "Enter father's month(mm)" : "Enter month(mm)");
         month = enteringBirthdayData(false,true);

        System.out.println(!child ? gender ? "Enter mother's day(dd)" :  "Enter father's day(dd)" : "Enter day(dd)");
         day = enteringBirthdayData(false,false);

        String concatBirthday = day + "/" + month + "/" + year;

        System.out.println(!child ? gender ? "Enter mother's iq" :  "Enter father's iq" : "Enter iq");
         iq = enteringNumber(false,false);

        Human newHuman = gender ? new Woman(name,surname,iq,concatBirthday) : new Man(name,surname,iq,concatBirthday);
        return newHuman;
    }

    public static void generateForEditingCase(){
        String output = "1. Born child\n"+
                        "2. Adopt child\n"+
                        "3. Back to the menu";
        System.out.println(output);
    }

    public static void generateCommands(){
        String output = "1. Fill with test data\n" +
                "2. Display all families\n" +
                "3. Display families where the number of people is more than the specified\n" +
                "4. Display families where the number of people is less than the specified\n" +
                "5. Count families where number of people is equals to specified\n" +
                "6. Create new family\n" +
                "7. Delete family by index\n" +
                "8. Edit family by index\n" +
                "9. Delete all children older than";
        System.out.println(output);
    }
}
