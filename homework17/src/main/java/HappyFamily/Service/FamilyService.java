package HappyFamily.Service;

import HappyFamily.DAO.FamilyDao;
import HappyFamily.Exceptions.FamilyOverflowException;
import HappyFamily.People.*;
import HappyFamily.Pets.Pet;

import java.util.*;
import java.util.stream.IntStream;

public class FamilyService {
    private FamilyDao familyDao;
    private final static int MAX_SIZE_OF_FAMILY = 4;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        if(familyDao.getAllFamilies().size() == 0){
            System.out.println("No families in data base");
        }
        IntStream.range(0,familyDao.getAllFamilies().size())
            .mapToObj(i-> i+1 + ":" + familyDao.getFamilyByIndex(i).toString())
            .forEach(System.out::println);
    }


    public List<Family> getFamiliesLessThan (int number){
        List<Family> result = new ArrayList<>();
        familyDao.getAllFamilies()
                .stream()
                .filter(obj -> obj.countFamily() < number)
                .forEach(obj->{
                    System.out.println(obj);
                    result.add(obj);
                });
        if(result.size() == 0){
            System.out.println("null");
            return null;
        }
        return result;
    }

    public List<Family> getFamiliesBiggerThan(int number){
        List<Family> result = new ArrayList<>();
        familyDao.getAllFamilies()
                .stream()
                .filter(obj -> obj.countFamily() > number)
                .forEach(obj->{
                    System.out.println(obj);
                    result.add(obj);
                });
        if(result.size() == 0){
            System.out.println("null");
            return null;
        }
        return result;
    }

    public int countFamiliesWithMemberNumber (int number){
        long result = familyDao.getAllFamilies()
                .stream()
                .filter(obj -> obj.countFamily() == number)
                .count();
        return (int) result;
    }



    public void createNewFamily(Human mother,Human father){
        Family family = new Family(mother,father);
        familyDao.saveFamily(family);
    }


    public void deleteFamilyByIndex(int index){
        if(!validateIndex(index)) System.out.println("Invalid index");
        familyDao.deleteFamily(index);
    }


    public Family bornChild(Family family , String boyName , String girlName) throws FamilyOverflowException{
        Family result = null;
        try {
            if(family.countFamily() == 4) throw new FamilyOverflowException("Exception: Family is full - ");
            else{
                Human child = family.bornChild();
                if (child instanceof Man) {
                    child.setName(boyName);
                } else {
                    child.setName(girlName);
                }
            }
            result = family;
        }catch (FamilyOverflowException e){
            System.out.println(e.getMessage() + MAX_SIZE_OF_FAMILY + " members");
        }finally {
            return result;
        }
    }


    public Family adoptChild(Family family,Human child) throws FamilyOverflowException{
        Family result = null;
        try {
            if(family.countFamily() == 4) throw new FamilyOverflowException("Exception: Family is full - ");
            family.addChild(child);
            result = family;
        }catch (FamilyOverflowException e){
            System.out.println(e.getMessage() + MAX_SIZE_OF_FAMILY + " members");
        }finally {
            return result;
        }
    }


    public void deleteAllChildrenOlderThen(int age){
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int nowYear = cal.get(Calendar.YEAR);

        for(Family family : familyDao.getAllFamilies()){
            family.getChildren()
                .removeIf(child->{
                    cal.setTime(new Date(child.getBirthDate()));
                    int year = nowYear - cal.get(Calendar.YEAR);
                    return year > age;
                });
        }

    }


    public int count(){
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index){
        if(!validateIndex(index)) return null;
        return familyDao.getAllFamilies().get(index);
    }


    public Set<Pet> getPets(int index){
        if(!validateIndex(index)) return null;
        return familyDao.getAllFamilies().get(index).getPets();
    }

    public void addPet(int index,Pet pet){
        if(validateIndex(index)){
            familyDao.getAllFamilies().get(index).addPet(pet);
        }
    }


    public boolean validateIndex(int ind){
        return ind < familyDao.getAllFamilies().size() && ind >= 0;
    }

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

    public static int getMaxSizeOfFamily() {
        return MAX_SIZE_OF_FAMILY;
    }
}
