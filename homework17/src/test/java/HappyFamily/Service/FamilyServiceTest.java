package HappyFamily.Service;

import HappyFamily.DAO.*;
import HappyFamily.*;
import HappyFamily.People.*;
import HappyFamily.Pets.*;
import HappyFamily.Enums.*;

import org.junit.Before;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class FamilyServiceTest {
    Set<String> arr = new HashSet<String>();

    Map<String,String> motherScedule = new HashMap<String,String>();


    Map<String,String> fatherScedule = new HashMap<String,String>();


    Map<String,String> childScedule = new HashMap<String,String>();



    RoboCat cat = new RoboCat("Murka",5,55,arr);
    Dog dog = new Dog("Bobik",1,33,arr);

    Woman mother = new Woman("Mary", "Winchester","10/10/1974",90,motherScedule);

    Man father = new Man("John", "Winchester","10/10/1974",95,fatherScedule);

    Woman mother2 = new Woman("Jane", "Smith","10/10/1974",90,motherScedule);

    Man father2 = new Man("Bred", "Smith","10/10/1974",95,fatherScedule);

    Human child = new Man("Name", "Surname","10/10/1974",95,childScedule);
    Human child2 = new Man("Name2", "Surname","10/10/1974",95,childScedule);
    Human child3 = new Man("Name3", "Surname","10/10/1974",95,childScedule);


    FamilyDao dataBase = new CollectionFamilyDao();

    FamilyService service = new FamilyService(dataBase);

    @Before
    public void setUp(){
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");

        fatherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(),"visit parents");

        childScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(),"walk with mother");

        service.createNewFamily(mother,father);
    }

    @Test
    public void getAllFamilies() {
        assertTrue(service.getAllFamilies() == dataBase.getAllFamilies());
    }


    @Test
    public void getFamiliesLessThan() {
//        when
        List<Family> result = new ArrayList<>();
        for(Family family : service.getAllFamilies()){
            if(family.countFamily() < 3){
                result.add(family);
                System.out.println(family);
            }
        }
//        then
        assertEquals(result,service.getFamiliesLessThan(3));
    }

    @Test
    public void getFamiliesBiggerThan() {
//        given
        service.createNewFamily(mother2,father2);
//        when
        List<Family> result = new ArrayList<>();
        for(Family family : service.getAllFamilies()){
            if(family.countFamily() > 3){
                result.add(family);
                System.out.println(family);
            }
        }
//        then
        assertEquals(result,service.getFamiliesBiggerThan(3));
    }

    @Test
    public void countFamiliesWithMemberNumber() {
//        given
        service.createNewFamily(mother2,father2);
//        when
        service.adoptChild(service.getFamilyById(0),child);
//        then
        assertEquals(service.countFamiliesWithMemberNumber(2),1);
    }

    @Test
    public void createNewFamily() {
//        when
        service.createNewFamily(mother2,father2);
//        then
        assertEquals(service.getFamilyById(1).getMother(),mother2);
        assertEquals(service.getFamilyById(1).getFather(),father2);
        assertEquals(service.getAllFamilies().size(),2);
    }

    @Test
    public void deleteFamilyByIndex_Positive() {
//        when
        service.createNewFamily(mother2,father2);
        service.deleteFamilyByIndex(0);
//        then
        assertEquals(service.getFamilyById(0).getMother(),mother2);
        assertEquals(service.getFamilyById(0).getFather(),father2);
        assertEquals(service.getAllFamilies().size(),1);
    }

    @Test
    public void deleteFamilyByIndex_Negative() {
//        when
        service.createNewFamily(mother2,father2);
        service.deleteFamilyByIndex(3);
        service.deleteFamilyByIndex(-1);
//        then
        assertEquals(service.getFamilyById(0).getMother(),mother);
        assertEquals(service.getFamilyById(0).getFather(),father);
        assertEquals(service.getFamilyById(1).getMother(),mother2);
        assertEquals(service.getFamilyById(1).getFather(),father2);
        assertEquals(service.getAllFamilies().size(),2);
    }

    @Test
    public void bornChild() {
//        given
        Family family = service.getFamilyById(0);
//        when
        Family family1 = service.bornChild(family, "Jake","Kate");
//        then
        assertEquals(family,family1);
        assertTrue(family.getChildren().get(0).getName() == "Jake" || family.getChildren().get(0).getName() == "Kate" );
        assertTrue(service.getAllFamilies().get(0).getChildren().size() == 1);
        assertEquals(family.getChildren().get(0),service.getFamilyDao().getAllFamilies().get(0).getChildren().get(0));
    }

    @Test
    public void adoptChild() {
//        given
        Family family = service.getFamilyById(0);
//        when
        Family family1 = service.adoptChild(family, child);
//        then
        assertEquals(family,family1);
        assertTrue(service.getAllFamilies().get(0).getChildren().size() == 1);
        assertEquals(family.getChildren().get(0),service.getFamilyDao().getAllFamilies().get(0).getChildren().get(0));
    }

    @Test
    public void deleteAllChildrenOlderThen() {
//        given
        Family family = service.getFamilyById(0);
        service.adoptChild(family,child);
        service.adoptChild(family,child2);
        service.adoptChild(family,child3);

        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int nowYear = cal.get(Calendar.YEAR);
//        when
        service.deleteAllChildrenOlderThen(16);
//        then


        for(Family family1 : service.getFamilyDao().getAllFamilies()){
            Iterator<Human> i = family1.getChildren().iterator();
            while(i.hasNext()){
                Human child = i.next();
                cal.setTime(new Date(child.getBirthDate()));
                int year = nowYear - cal.get(Calendar.YEAR);
                assertTrue(year<=16);
            }
        }
    }

    @Test
    public void count() {
        assertTrue(service.getAllFamilies().size() == service.count());
    }

    @Test
    public void getFamilyById() {
        Family family = service.getAllFamilies().get(0);
        Family family1 = service.getFamilyById(0);
        assertEquals(family,family1);
    }

    @Test
    public void addPet() {
//        when
        service.addPet(0,dog);
//        then
        assertTrue(service.getFamilyById(0).getPets().contains(dog));
    }

    @Test
    public void getPets() {
        assertEquals(service.getPets(0),service.getFamilyById(0).getPets());
    }

}