package HappyFamily.People;


import java.io.Serializable;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.LocalDate;
import java.time.Period;
import java.time.ZoneId;
import java.util.Date;
import java.util.Map;
import java.util.Objects;
import java.util.Random;

import HappyFamily.Pets.*;

public class Human implements Serializable {
    protected String name;
    protected String surname;
    protected long birthDate;
    protected int iq;
    protected Family family;
    protected Map<String, String> scedule;
    protected static final SimpleDateFormat SDF =  new SimpleDateFormat("dd/MM/yyyy");

    static {
        System.out.println("Human initialised");
    }

    {
        System.out.println("New Human`s object is creating");
    }


    public Human(String name, String surname, String birthDate, int iq, Map<String, String> scedule) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.scedule = scedule;
        try{
            this.birthDate = SDF.parse(birthDate).getTime();
        }catch (ParseException e){
            System.out.println(e);
        }
    }

    public Human(String name, String surname, int iq, String birthDate, Family family) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.family = family;
        try{
            this.birthDate = SDF.parse(birthDate).getTime();
        }catch (ParseException e){
            System.out.println(e);
        }
    }

    public Human(String name, String surname, int iq, String birthDate) {
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        try{
            this.birthDate = SDF.parse(birthDate).getTime();
        }catch (ParseException e){
            System.out.println(e);
        }
    }



    public Human() {
    }

    //    methods

    public String describeAge(){
        Date bday = new Date(birthDate);
        Period period = Period.between(convertToLocalDate(bday), convertToLocalDate(new Date()));
        return "Years: " + period.getYears() + ", Months: " + period.getMonths() + ", Days: " + period.getDays();
    }

    public String birthdateToStr(){
        return SDF.format(new Date(birthDate));
    }

    public LocalDate convertToLocalDate(Date date){
        return date.toInstant().atZone(ZoneId.systemDefault()).toLocalDate();
    }

    protected void finalize() {
        System.out.println(name + " " + surname + " is deleting");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return name.hashCode() + surname.hashCode() + (int) birthDate * 31;
    }


    public String greetPet() {
        String result = "";
        if (getFamily() == null) return null;
        if (getFamily().getPets() != null) {
            for (Pet pet : family.getPets()) {
                result += "Привет, " + pet.getNickname() + "\n";
            }
        } else result = "Надо завести животное , чтобы его поприветствовать";
        return result;
    }

    public String describePet() {
        if (getFamily() == null) return null;
        if (getFamily().getPets() != null) {
            String trick = "";
            String output = "";
            for (Pet pet : family.getPets()) {
                if (pet.getTrickLevel() > 50) {
                    trick = "очень хитрый";
                } else {
                    trick = "почти не хитрый";
                }
                output += "У меня есть " + pet.getNickname() + " ему " + pet.getAge() + " лет, он " + trick + "\n";
            }
            return output;
        } else return "Надо завести животное , чтобы его можно было отписать";
    }

    public boolean feedPet(boolean flag) {
        if (getFamily() == null) return false;
        if (getFamily().getPets() != null) {
            String output = "";
            boolean result = false;
            if (flag) {
                for (Pet pet : family.getPets()) {
                    output += "Хм... покормлю ка я " + pet.getNickname() + "\n";
                    result = true;
                }

            } else {
                Random ran = new Random();
                int random = ran.nextInt(101);
                for (Pet pet : family.getPets()) {
                    if (pet.getTrickLevel() > random) {
                        output += "Хм... покормлю ка я " + pet.getNickname() + "\n";
                        result = true;
                    } else {
                        output += "Думаю, " + pet.getNickname() + " не голоден." + "\n";
                    }
                }

            }
            System.out.println(output);
            return result;
        } else {
            System.out.println("Надо завести животное , чтобы его покормить");
            return false;
        }
    }

    public void addToScedule(String day, String business) {
        scedule.put(day, business);
    }

    public String toString() {
        String output = "{name = " + name + ", surname = " + surname + ", birthDate = " + birthdateToStr() + ", iq = " + iq + ", scedule = " + scedule + "}";
        return output;
    }

//    getters/setters


    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public Map<String, String> getScedule() {
        return scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public long getBirthDate() {
        return birthDate;
    }

    public void setBirthDate(int birthDate) {
        this.birthDate = birthDate;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setScedule(Map<String, String> scedule) {
        this.scedule = scedule;
    }

}
