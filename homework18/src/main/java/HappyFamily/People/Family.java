package HappyFamily.People;


import java.io.Serializable;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.stream.IntStream;
import java.util.stream.Stream;

import HappyFamily.Pets.*;

public class Family implements HumanCreator, Serializable {
    private Human mother;
    private Human father;
    private List<Human> children = new ArrayList<Human>();
    private Set<Pet> pets = new HashSet<Pet>();

    static {
        System.out.println("Family initialised");
    }

    {
        System.out.println("New Family`s object is creating");
    }


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }


//    methods

    public void addPet(Pet pet){
        pets.add(pet);
    }

    public Human bornChild() {
        Human child = new Human();
        Random ran = new Random();

        Date date = new Date();

        String year = new SimpleDateFormat("dd/MM/yyyy").format(new Date());

        int random = ran.nextInt(2);

        int iq = (father.getIq() + mother.getIq()) / 2;

        String[][] names = {
                {"Alex","Dima","Kostya","Sam","Dean","John","Max","Igor","Michael"},
                {"Alex","Sonya","Jane","Mary","July","Maria","Zhenya","Katya","Anya"}
        };

        int nameRandom = ran.nextInt(names[random].length);
        String name = names[random][nameRandom];

//        0 - man , 1 - woman
        if(random == 0){
            child = new Man(name,father.getSurname(),iq,year,this);
        }
        else {
            child = new Woman(name,father.getSurname(),iq,year,this);
        }
        addChild(child);
        return child;
    }



    protected void finalize () {
        System.out.println (toString() + " is deleting");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = mother.hashCode() + father.hashCode() * 31;
        return result;
    }

    @Override
    public String toString() {
        String[] childrenStr = new String[children.size()];
        String[] petsStr = new String[pets.size()];
        IntStream.range(0,children.size())
                .forEach(i->{
                    childrenStr[i]="\n\t\t\t" +(children.get(i) instanceof Man ? "boy:" : "girl:") + children.get(i).toString();
                });
        int i = 0;
        for(Pet pet : pets){
            petsStr[i]=pet.toString();
            i++;
        }
        return "family:" +
                " \n \t\tmother= " + mother.toString() +
                ",\n \t\tfather= " + father.toString() +
                ",\n \t\tchiildren:" + (children.size() == 0 ? null :Arrays.toString(childrenStr).replaceAll("(\\[)|(\\])"," ")) +
                "\n \t\tpets=" + Arrays.toString(petsStr);
    }

    public void addChild(Human child){
        child.setFamily(this);
        children.add(child);
    }

    public boolean deleteChild(int index){
        if(isNotEmpty() && index <= children.size()-1){
            children.remove(index);
            return true;
        }
        else return false;
    }

    public void deleteChild(Human child){
        if(isNotEmpty() && children.indexOf(child) != -1){
            children.remove(child);
        }
    }

    public boolean isNotEmpty(){
        return children != null && children.size() != 0;
    }


    public int countFamily(){
        int result = 2;
        if(children == null) return result;
        result += children.size();
        return result;
    }


    //    getters/setters
    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public List<Human> getChildren() {
        return children;
    }

    public Set<Pet> getPets() {
        return pets;
    }

    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChiildren(List<Human> children) {
        this.children = children;
    }
}
