package HappyFamily.Pets;

import HappyFamily.Enums.Species;

import java.util.Objects;
import java.util.Set;

public class Dog extends Pet implements Foul{

    public Dog(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname,age,trickLevel,habits);
        species = Species.DOG;
    }

    public String foul(){
        return "Нужно хорошо замести следы...";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return  Objects.equals(species, dog.species) &&
                Objects.equals(nickname, dog.nickname);
    }

    @Override
    public int hashCode() {
        return species.hashCode() + nickname.hashCode() + age * 31;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + nickname + ". Я соскучился!";
    }


}
