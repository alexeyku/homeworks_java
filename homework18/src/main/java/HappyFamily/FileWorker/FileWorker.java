package HappyFamily.FileWorker;

import HappyFamily.People.Family;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.ArrayList;
import java.util.List;

public class FileWorker {
    private final static String fileRouteToFileWithObjects = "src/FamilyData.ser";
    private final static String fileRouteToFileWithObjectsStr = "src/FamilyDataStr.txt";
    private final static File fileWithObjects = new File(fileRouteToFileWithObjects);
    private final static File fileWithObjectsStr = new File(fileRouteToFileWithObjectsStr);

    public void loadData(List<Family> families){
        try{
            if(!fileWithObjects.exists()){
                fileWithObjects.createNewFile();
            }
            if(families != null && families.size() > 0){
                loadDataStr(families);
                FileOutputStream fileOut = new FileOutputStream(fileRouteToFileWithObjects);
                ObjectOutputStream out = new ObjectOutputStream(fileOut);
                out.writeObject(families);
                out.close();
                fileOut.close();
                System.out.println("Serialized data to file");
            }
            else{
                System.out.println("No data to load");
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public List<Family> downloadData(){
        List<Family> families = new ArrayList<>();
        try{
            if(!fileWithObjects.exists() || fileWithObjects.length() <= 0){
                System.out.println("No data in file. Please load data then try to download it again");
                return null;
            }
            else {
                FileInputStream fileIn = new FileInputStream(fileRouteToFileWithObjects);
                ObjectInputStream objectIn = new ObjectInputStream(fileIn);
                families = (ArrayList<Family>) objectIn.readObject();
                objectIn.close();
                fileIn.close();
            }

        }  catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return families;
    }

    private void loadDataStr(List<Family> families){
        try{
            if(fileWithObjectsStr.exists()){
                PrintWriter writer = new PrintWriter(fileWithObjectsStr);
                writer.print("");
                writer.close();
            }
            if(!fileWithObjectsStr.exists()){
                fileWithObjectsStr.createNewFile();
            }
            String text = null;
            for (int i = 0; i < families.size(); i++) {
                if (text == null) text = (i+1) + ":" + families.get(i).toString() + "\n";
                else{
                    text += (i+1) + ":" + families.get(i).toString() + "\n";
                }
            }
            Files.write(Paths.get(fileRouteToFileWithObjectsStr), text.getBytes(), StandardOpenOption.APPEND);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
