package HappyFamily.DAO;

import HappyFamily.People.Family;
import HappyFamily.Service.LoggerService;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList();

    public List<Family> getAllFamilies() {
        LoggerService.info("Get All Families");
        return families;
    }


    public Family getFamilyByIndex(int index) {
        LoggerService.info("Get Family by index " + index);
        return index > families.size()-1 || index<0 ? null : families.get(index);
    }

    public boolean deleteFamily(int index) {
        LoggerService.info("Deleted Family by index " + index);
        if (index > families.size() - 1 || index<0) return false;
        families.remove(index);
        return true;
    }

    public boolean deleteFamily(Family family) {
        LoggerService.info("Deleted Family by Family");
        boolean result = false;
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).equals(family)) {
                families.remove(i);
                result = true;
            }
        }
        return result;
    }

    public void saveFamily(Family family) {
        LoggerService.info("Saved Family");
        boolean flag = false;
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).equals(family)) {
                families.set(i, family);
                flag = true;
                break;
            }
        }
        if(!flag) families.add(family);
    }

}
