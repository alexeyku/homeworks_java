package HappyFamily.People;


import HappyFamily.Enums.*;
import org.junit.Test;

import java.util.*;

import static org.junit.Assert.*;

public class HumanTest {

    Set<String> arr = new HashSet<String>();

    Map<String,String> motherScedule = new HashMap<String,String>();

    Human mother = new Human("Mary", "Winchester","10/10/1974",90,motherScedule);


    protected void setUp() throws Exception{
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");
    }

    @Test
    public void testToString() {
        assertEquals(getExpextedToString(mother),mother.toString());

        Human human = new Human("Alex", "Winch","10/10/1974",80,motherScedule);
        assertEquals(getExpextedToString(human),human.toString());

    }


    @Test
    public void testEquals() {
//        1. obj == obj2
        Human mother2 = mother;
        assertTrue(mother.equals(mother2));
        assertTrue(mother2.equals(mother));

//        2.

//            2.1 obj2 == null
        mother2 = null;
        assertFalse(mother.equals(mother2));

//            2.2 obj.getClass() != obj2.getClass()
//        Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);
//        assertFalse(mother.equals(dog));

//        3. fields comparison
        mother2 = new Human("Mary", "NotWinchester","10/10/1974",90,motherScedule);
        assertFalse(mother.equals(mother2));

        mother2 = new Human("Mary", "Winchester","10/10/1974",100,motherScedule);
        assertTrue(mother.equals(mother2));
    }

    @Test
    public void testHashCode(){
        Human mother2 = new Human("Mary", "Winchester","10/10/1974",100,motherScedule);
        if(mother.equals(mother2)){
            assertTrue(mother.hashCode() == mother2.hashCode());
        }

    }



    public String getExpextedToString(Human target){
        return  "{name = " + target.getName() + ", surname = " + target.getSurname() + ", birthDate = " + target.birthdateToStr() + ", iq = " + target.getIq() + ", scedule = " + target.getScedule() + "}";
    }
}