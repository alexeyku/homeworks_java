package HappyFamily.People;

import java.util.Map;

import HappyFamily.Pets.*;

final public class Woman extends Human{

    public Woman(String name, String surname, String birthDate , int iq, Map<String,String> scedule){
        super(name,surname,birthDate,iq,scedule);
    }

    public Woman(String name, String surname, int iq, String birthDate, Family family){
        super(name,surname,iq,birthDate,family);
    }

    public Woman(String name, String surname, int iq, String birthDate){
        super(name,surname,iq,birthDate);
    }

    @Override
    public String greetPet(){
        if(family == null) return null;
        String output = "";
        if(family.getPets() != null){
            for(Pet pet : family.getPets()){
                output+= "Привееет, " + pet.getNickname() + "))) \n";
            }

        }
        else output =  "Нету животного, нужно завести...";
        return output;
    }

    public String makeup(){
        return "Через 2 минуты буду готова, честное слово";
    }
}
