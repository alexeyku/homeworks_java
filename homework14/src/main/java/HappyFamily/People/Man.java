package HappyFamily.People;

import HappyFamily.Pets.Pet;

import java.util.Map;

final public class Man extends Human{

    public Man(String name, String surname, String birthDate , int iq, Map<String,String> scedule){
        super(name,surname,birthDate,iq,scedule);
    }

    public Man(String name, String surname, int iq, String birthDate, Family family){
        super(name,surname,iq,birthDate,family);
    }

    public Man(String name, String surname, int iq, String birthDate){
        super(name,surname,iq,birthDate);
    }

    @Override
    public String greetPet(){
        if(family == null) return null;
        String output = "";
        if(family.getPets() != null){
            for(Pet pet : family.getPets()){
                output+= "Привееет, " + pet.getNickname() + "))) \n";
            }

        }
        else  output = "Нету животного";
        return output;
    }

    public String repairCar(){
        return "Пойду-ка я чинить машину" ;
    }

}
