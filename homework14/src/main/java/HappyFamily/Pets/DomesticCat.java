package HappyFamily.Pets;

import HappyFamily.Enums.Species;


import java.util.Objects;
import java.util.Set;

public class DomesticCat extends Pet implements Foul{

    public DomesticCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname,age,trickLevel,habits);
        species = Species.DOMESTICCAT;
    }

    public String foul(){
        return "Нужно хорошо замести следы...";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        DomesticCat domesticCat = (DomesticCat) o;
        return  Objects.equals(species, domesticCat.species) &&
                Objects.equals(nickname, domesticCat.nickname);
    }

    public int hashCode() {
        return species.hashCode() + nickname.hashCode() + age * 31;
    }

    @Override
    public String toString() {
        String output = species.getDescription() + "{\n\tnickname = '" + nickname + "', age = " + age + ", trickLevel = " + trickLevel + ", can fly = " + species.isCanFly() + ", number of legth = " + species.getNumberOfLegth() + ", has fur = " + species.isHasFur()  + ", habits = " + habits+ "\n\t}";
        return output;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + nickname + ". Я соскучился!";
    }


}
