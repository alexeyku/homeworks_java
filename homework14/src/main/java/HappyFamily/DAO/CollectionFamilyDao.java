package HappyFamily.DAO;

import HappyFamily.People.Family;

import java.util.ArrayList;
import java.util.List;

public class CollectionFamilyDao implements FamilyDao {
    private List<Family> families = new ArrayList<Family>();

    public List<Family> getAllFamilies() {
        return families;
    }


    public Family getFamilyByIndex(int index) {
        return index > families.size()-1 || index<0 ? null : families.get(index);
    }

    public boolean deleteFamily(int index) {
        if (index > families.size() - 1 || index<0) return false;
        families.remove(index);
        return true;
    }

    public boolean deleteFamily(Family family) {
        boolean result = false;
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).equals(family)) {
                families.remove(i);
                result = true;
            }
        }
        return result;
    }

    public void saveFamily(Family family) {
        boolean flag = false;
        for (int i = 0; i < families.size(); i++) {
            if (families.get(i).equals(family)) {
                families.set(i, family);
                flag = true;
                break;
            }
        }
        if(!flag) families.add(family);
    }

}
