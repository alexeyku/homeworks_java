import java.util.Scanner;
import java.lang.String;

public class Scedule {
    public static void main(String[] args) {
        String[][] scedule = new String[7][2];

        scedule[0][0] = "Sunday";
        scedule[0][1] = "do home work";

        scedule[1][0] = "Monday";
        scedule[1][1] = "go to courses; watch a film";

        scedule[2][0] = "Tuesday";
        scedule[2][1] = "go to the theatre";

        scedule[3][0] = "Wednesday";
        scedule[3][1] = "go walk";

        scedule[4][0] = "Thursday";
        scedule[4][1] = "dinner with parents";

        scedule[5][0] = "Friday";
        scedule[5][1] = "drink alcohol";

        scedule[6][1] = "relax after friday";


        Scanner in = new Scanner(System.in);
        label:
        for (;;) {
            System.out.println("Please, input the day of the week:");
            String answer = in.nextLine();

            String changedDay = "";
            if(answer.toLowerCase().trim().contains("change ")){

                answer = answer.trim().toLowerCase();
                changedDay = answer.substring(answer.indexOf(" "),answer.length()).trim();
                changedDay = changedDay.substring(0, 1).toUpperCase() + changedDay.substring(1);

                for (int i = 0; i < scedule.length-1; i++) {
                    if(scedule[i][0].equals(changedDay)){
                        answer = "change";
                    }
                }

            }


            switch (answer.toLowerCase().trim()){
                case "monday" :
                    System.out.println(scedule[1][1]);
                    break;

                case "tuesday" :
                    System.out.println(scedule[2][1]);
                    break;

                case "wednesday":
                    System.out.println(scedule[3][1]);
                    break;

                case "thursday":
                    System.out.println(scedule[4][1]);
                    break;

                case "friday":
                    System.out.println(scedule[5][1]);
                    break;

                case "saturday":
                    System.out.println(scedule[6][1]);
                    break;

                case "sunday":
                    System.out.println(scedule[0][1]);
                    break;

                case "change":
                    System.out.println("Please, input new tasks for " + changedDay);
                    String newTask = in.nextLine();
                    for (int i = 0; i < scedule.length-1; i++) {
                        if(scedule[i][0].equals(changedDay)){
                            scedule[i][1] = newTask;
                        }
                    }
                    break;

                case "exit" :
                    break label;

                default:
                    System.out.println("Sorry, I don't understand you, please try again.");
                    break;
            }
        }
    }
}
