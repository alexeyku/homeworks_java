package HappyFamily.People;


import HappyFamily.DayOfWeek;
import HappyFamily.Species;
import HappyFamily.Pets.*;
import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class HumanTest {
    String[] arr = {"eat, drink, sleep"};
    String[][] motherScedule = {
            {DayOfWeek.SATURDAY.name(),"relax with family"},
            {DayOfWeek.SUNDAY.name(),"walk with child"},
    };


    Human mother = new Human("Mary", "Winchester",1977,90,motherScedule);


    @Test
    public void testToString() {
        assertEquals(getExpextedToString(mother),mother.toString());

        Human human = new Human("Alex", "Winch",2000,80,motherScedule);
        assertEquals(getExpextedToString(human),human.toString());

    }


    @Test
    public void testEquals() {
//        1. obj == obj2
        Human mother2 = mother;
        assertTrue(mother.equals(mother2));
        assertTrue(mother2.equals(mother));

//        2.

//            2.1 obj2 == null
        mother2 = null;
        assertFalse(mother.equals(mother2));

//            2.2 obj.getClass() != obj2.getClass()
//        Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);
//        assertFalse(mother.equals(dog));

//        3. fields comparison
        mother2 = new Human("Mary", "NotWinchester",1977,90,motherScedule);
        assertFalse(mother.equals(mother2));

        mother2 = new Human("Mary", "Winchester",1977,100,motherScedule);
        assertTrue(mother.equals(mother2));
    }

    @Test
    public void testHashCode(){
        Human mother2 = new Human("Mary", "Winchester",1977,100,motherScedule);
        if(mother.equals(mother2)){
            assertTrue(mother.hashCode() == mother2.hashCode());
        }

    }



    public String getExpextedToString(Human target){
        String output = "\n" + target.getClass().getSimpleName() + "{\n name = " + target.getName() + ",\n surname = " + target.getSurname() + ",\n year = " + target.getYear() + ",\n iq = " + target.getIq() + ",\n scedule = " + Arrays.deepToString(target.getScedule()) + "\n}";
        return output;
    }
}