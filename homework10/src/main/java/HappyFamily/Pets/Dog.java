package HappyFamily.Pets;

import HappyFamily.Species;

import java.util.Arrays;
import java.util.Objects;

public class Dog extends Pet implements Foul{
    private Species species;

    public Dog(String nickname, int age, int trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.species = Species.DOG;
    }

    @Override
    public String foul(){
        return "Нужно хорошо замести следы...";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Dog dog = (Dog) o;
        return  Objects.equals(species, dog.species) &&
                Objects.equals(getNickname(), dog.getNickname());
    }

    @Override
    public int hashCode() {
        return species.hashCode() + getNickname().hashCode() + getAge() * 31;
    }

    @Override
    public String toString() {
        String output = species.getDescription() + "{\n\tnickname = '" + getNickname() + "', age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", can fly = " + (species != Species.UNKNOWN ? species.isCanFly() : null ) + ", number of legth = " + (species != Species.UNKNOWN ? species.getNumberOfLegth() : null ) + ", has fur = " + (species != Species.UNKNOWN ? species.isHasFur() : null )  + ", habits = " + Arrays.toString(getHabits())+ "\n\t}";
        return output;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + getNickname() + ". Я соскучился!";
    }


}
