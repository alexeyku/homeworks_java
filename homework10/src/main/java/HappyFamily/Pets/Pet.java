package HappyFamily.Pets;


import HappyFamily.Species;

public abstract class Pet {
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;
    private Species species = Species.UNKNOWN;

    static {
        System.out.println("Pet initialised");
    }

    {
        System.out.println("New Pet`s object is creating");
    }

    public Pet(String nickname, int age, int trickLevel, String[] habits) {
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    protected void finalize () {
        System.out.println (nickname + " is deleting");
    }

    public String eat() {
        return  "Я кушаю!";
    }

    public abstract String respond();


//    getters/setters

    public Species getSpecies(){
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
