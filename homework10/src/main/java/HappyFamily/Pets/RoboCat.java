package HappyFamily.Pets;

import HappyFamily.Species;

import java.util.Arrays;
import java.util.Objects;

public class RoboCat extends Pet implements Foul{
    private Species species;

    public RoboCat(String nickname, int age, int trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.species = Species.ROBOCAT;
    }

    @Override
    public String foul(){
        return "Нужно хорошо замести следы...";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoboCat robocat = (RoboCat) o;
        return  Objects.equals(species, robocat.species) &&
                Objects.equals(getNickname(), robocat.getNickname());
    }

    @Override
    public int hashCode() {
        return species.hashCode() + getNickname().hashCode() + getAge() * 31;
    }


    @Override
    public String toString() {
        String output = species.getDescription() + "{\n\tnickname = '" + getNickname() + "', age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", can fly = " + species.isCanFly() + ", number of legth = " + species.getNumberOfLegth() + ", has fur = " + species.isHasFur()  + ", habits = " + Arrays.toString(getHabits())+ "\n\t}";
        return output;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + getNickname() + ". Я соскучился!";
    }

}
