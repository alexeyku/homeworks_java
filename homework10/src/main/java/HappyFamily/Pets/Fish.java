package HappyFamily.Pets;

import HappyFamily.Species;

import java.util.Arrays;
import java.util.Objects;

public class Fish extends Pet {
    private Species species;

    public Fish(String nickname, int age, int trickLevel, String[] habits){
        super(nickname,age,trickLevel,habits);
        this.species = Species.FISH;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + getNickname() + ". Я соскучился!";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Fish fish = (Fish) o;
        return  Objects.equals(species, fish.species) &&
                Objects.equals(getNickname(), fish.getNickname());
    }

    @Override
    public int hashCode() {
        return species.hashCode() + getNickname().hashCode() + getAge() * 31;
    }

    @Override
    public String toString() {
        String output = species.getDescription() + "{\n\tnickname = '" + getNickname() + "', age = " + getAge() + ", trickLevel = " + getTrickLevel() + ", can fly = " + species.isCanFly() + ", number of legth = " + species.getNumberOfLegth() + ", has fur = " + species.isHasFur()  + ", habits = " + Arrays.toString(getHabits())+ "\n\t}";
        return output;
    }

}
