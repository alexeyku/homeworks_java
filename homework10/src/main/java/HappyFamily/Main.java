package HappyFamily;

import HappyFamily.People.*;
import HappyFamily.Pets.*;

public class Main {
    public static void main(String[] args) {
        String[] arr = {"eat, drink, sleep"};
        String[][] motherScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"walk with child"},
        };
        String[][] fatherScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"visit parents"}
        };

        String[][] childScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"walk with mother"}
        };

        RoboCat cat = new RoboCat("Murka",5,55,arr);

        Woman mother = new Woman("Mary", "Winchester",1977,90,motherScedule);

        Man father = new Man("John", "Winchester",1974,95,fatherScedule);

        Family family = new Family(mother,father);

        family.setPet(cat);

        System.out.println(mother.greetPet());


        Human child = family.bornChild();
        child.setScedule(childScedule);
        System.out.println(child.getFamily());
        System.out.println(mother.makeup());
        System.out.println(father.repairCar());



    }




}
