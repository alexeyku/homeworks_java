package HappyFamily.People;


import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] scedule;

    static {
        System.out.println("Human initialised");
    }

    {
        System.out.println("New Human`s object is creating");
    }


    public Human(String name, String surname, int year , int iq, String[][] scedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.scedule = scedule;
    }

    public Human(String name, String surname, int iq, int year, Family family){
        this.name = name;
        this.surname = surname;
        this.iq = iq;
        this.year = year;
        this.family = family;
    }

//    methods

    protected void finalize () {
        System.out.println(name + " " + surname + " is deleting");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o)return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname);
    }

    @Override
    public int hashCode() {
        return name.hashCode() + surname.hashCode() + year *31;
    }



    public String greetPet(){
        if(getFamily() == null) return null;
        if(getFamily().getPet() != null){
            return "Привет, " + getFamily().getPet().getNickname();
        }
        else return "Надо завести животное , чтобы его поприветствовать";
    }

    public String describePet(){
        if(getFamily() == null) return null;
        if(getFamily().getPet() != null){
            String trick = "";
            if(getFamily().getPet().getTrickLevel() > 50 ){
                trick = "очень хитрый";
            }
            else{
                trick = "почти не хитрый";
            }
            String output = "У меня есть " + getFamily().getPet().getSpecies() + " , ему " + getFamily().getPet().getAge() + " лет, он " + trick;
            return output;
        }
        else return "Надо завести животное , чтобы его можно было отписать";
    }

    public boolean feedPet(boolean flag){
        if(getFamily() == null) return false;
        if(getFamily().getPet() != null){
            String output = "";
            boolean result = false;
            if(flag){
                output = "Хм... покормлю ка я " + getFamily().getPet().getNickname();
                result = true;
            }
            else{
                Random ran = new Random();
                int random = ran.nextInt(101);
                if(getFamily().getPet().getTrickLevel() > random){
                    output = "Хм... покормлю ка я " + getFamily().getPet().getNickname();
                    result = true;
                }
                else{
                    output = "Думаю, " + getFamily().getPet().getNickname() + " не голоден.";
                }
            }
            System.out.println(output);
            return result;
        }
        else{
            System.out.println("Надо завести животное , чтобы его покормить");
            return false;
        }
    }



    public String toString(){
        String output = "\n" + this.getClass().getSimpleName() + "{\n name = " + name + ",\n surname = " + surname + ",\n year = " + year + ",\n iq = " + iq + ",\n scedule = " + Arrays.deepToString(scedule) + "\n}";
        return output;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return family;
    }

    public String[][] getScedule() {
        return scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setScedule(String[][] scedule) {
        this.scedule = scedule;
    }


}
