package HappyFamily.People;

final public class Man extends Human{

    public Man(String name, String surname, int year , int iq, String[][] scedule){
        super(name,surname,year,iq,scedule);
    }

    public Man(String name, String surname, int iq, int year, Family family){
        super(name,surname,iq,year,family);
    }

    @Override
    public String greetPet(){
        if(getFamily() == null) return null;
        if(getFamily().getPet() != null){
            return "Привееет, " + getFamily().getPet().getNickname() + "))) ";
        }
        else return "Нету животного";
    }

    public String repairCar(){
        return "Пойду-ка я чинить машину" ;
    }

}
