package HappyFamily.People;

final public class Woman extends Human{

    public Woman(String name, String surname, int year , int iq, String[][] scedule){
        super(name,surname,year,iq,scedule);
    }

    public Woman(String name, String surname, int iq, int year, Family family){
        super(name,surname,iq,year,family);
    }

    @Override
    public String greetPet(){
        if(getFamily() == null) return null;
        if(getFamily().getPet() != null){
            return "Привееет, " + getFamily().getPet().getNickname() + "))) ";
        }
        else return "Нету животного, нужно завести...";
    }

    public String makeup(){
        return "Через 2 минуты буду готова, честное слово";
    }
}
