package HappyFamily.DAO;

import HappyFamily.*;
import HappyFamily.People.*;
import HappyFamily.Pets.*;

import org.junit.Test;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

import static org.junit.Assert.*;

public class CollectionFamilyDaoTest {
    Set<String> arr = new HashSet<String>();

    Map<String,String> motherScedule = new HashMap<String,String>();

    Map<String,String> fatherScedule = new HashMap<String,String>();


    Map<String,String> childScedule = new HashMap<String,String>();

    RoboCat cat = new RoboCat("Murka",5,55,arr);
    Dog dog = new Dog("Bobik",1,33,arr);

    Woman mother = new Woman("Mary", "Winchester",1977,90,motherScedule);

    Man father = new Man("John", "Winchester",1974,95,fatherScedule);

    Woman mother2 = new Woman("Jane", "Winchester",1977,90,motherScedule);

    Man father2 = new Man("John", "Winchester",1974,95,fatherScedule);

    Family family = new Family(mother,father);
    Family family2 = new Family(mother2,father2);


    protected void setUp(){
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

    }

    @Test
    public void getFamilyByIndex_ShouldReturnNull(){
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
//        then
        assertNull(dataBase.getFamilyByIndex(0));
    }

        @Test
    public void getFamilyByIndex_ShouldReturnObj(){
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
//        when
            dataBase.saveFamily(family);
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
    }



    @Test
    public void testSaveFamilyWhenEmpty() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
//        when
        dataBase.saveFamily(family);
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
    }

      @Test
    public void testSaveFamilyWhenNotEmpty() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
//        when
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
        assertTrue(dataBase.getFamilyByIndex(1).equals(family2));
    }

  @Test
    public void testResetFamily() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
//        when
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
        dataBase.saveFamily(family2);
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
        assertTrue(dataBase.getFamilyByIndex(1).equals(family2));
        assertNull(dataBase.getFamilyByIndex(2));
    }


    @Test
    public void deleteFamilyByIndexPositive() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
//        when
        assertTrue(dataBase.deleteFamily(0));
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family2));
        assertTrue(dataBase.getAllFamilies().size() == 1);
    }

        @Test
    public void deleteFamilyByIndexNegative() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
//        when
        assertFalse(dataBase.deleteFamily(2));
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
        assertTrue(dataBase.getFamilyByIndex(1).equals(family2));
        assertTrue(dataBase.getAllFamilies().size() == 2);
    }



    @Test
    public void deleteFamilyByFamilyObjPositive() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
//        when
        assertTrue(dataBase.deleteFamily(family));
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family2));
        assertTrue(dataBase.getAllFamilies().size() == 1);
    }

    @Test
    public void deleteFamilyByFamilyObjNegative() {
//        given
        FamilyDao dataBase = new CollectionFamilyDao();
        dataBase.saveFamily(family);
//        when
        assertFalse(dataBase.deleteFamily(family2));
//        then
        assertTrue(dataBase.getFamilyByIndex(0).equals(family));
        assertTrue(dataBase.getAllFamilies().size() == 1);
    }

}