package HappyFamily;

import HappyFamily.People.*;
import HappyFamily.Pets.*;
import HappyFamily.DAO.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

        Map<String,String> motherScedule = new HashMap<String,String>();
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");

        Map<String,String> fatherScedule = new HashMap<String,String>();
        fatherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(),"visit parents");

        Map<String,String> childScedule = new HashMap<String,String>();
        childScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(),"walk with mother");


        RoboCat cat = new RoboCat("Murka",5,55,arr);
        Dog dog = new Dog("Bobik",1,33,arr);

        Woman mother = new Woman("Mary", "Winchester",1977,90,motherScedule);

        Man father = new Man("John", "Winchester",1974,95,fatherScedule);

        Woman mother2 = new Woman("Jane", "Smith",1977,90,motherScedule);

        Man father2 = new Man("Bred", "Smith",1974,95,fatherScedule);

        Family family = new Family(mother,father);
        Family family2 = new Family(mother2,father2);


        FamilyDao dataBase = new CollectionFamilyDao();
        dataBase.saveFamily(family);
        dataBase.saveFamily(family2);
        dataBase.saveFamily(family);
        Human child = family2.bornChild();
        dataBase.saveFamily(family2);
        dataBase.saveFamily(family);
        System.out.println(dataBase.getAllFamilies());
        System.out.println("size(expected:2): " +dataBase.getAllFamilies().size());

        System.out.println("is deleted(expexted:true): " + dataBase.deleteFamily(0));
        System.out.println("is deleted(expexted:true): " + dataBase.deleteFamily(family2));
        System.out.println("is deleted(expexted:false): " + dataBase.deleteFamily(family2));
        System.out.println("size(expected:0): " +dataBase.getAllFamilies().size());

    }

}
