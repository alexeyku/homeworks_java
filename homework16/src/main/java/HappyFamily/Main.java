package HappyFamily;

import HappyFamily.Controller.FamilyController;
import HappyFamily.Enums.DayOfWeek;
import HappyFamily.People.*;
import HappyFamily.Pets.*;
import HappyFamily.DAO.*;
import HappyFamily.Service.FamilyService;

import java.util.*;

public class Main {
    public final static Scanner in = new Scanner(System.in);
    public static void main(String[] args) {
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

        Map<String, String> motherScedule = new HashMap<String, String>();
        motherScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(), "walk with child");

        Map<String, String> fatherScedule = new HashMap<String, String>();
        fatherScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(), "visit parents");

        Map<String, String> childScedule = new HashMap<String, String>();
        childScedule.put(DayOfWeek.SATURDAY.name(), "relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(), "walk with mother");

        Woman mother = new Woman("Mary", "Winchester", "10/10/1974", 90, motherScedule);
        Man father = new Man("John", "Winchester", "22/05/1985", 95, fatherScedule);

        Woman mother2 = new Woman("Jane", "Smith", "14/07/1980", 90, motherScedule);
        Man father2 = new Man("Bred", "Smith", "27/09/1982", 95, fatherScedule);

        Woman mother3 = new Woman("Kate", "Sur", "14/07/1980", 90, motherScedule);
        Man father3 = new Man("Bred", "Sur", "27/09/1982", 95, fatherScedule);

        Woman mother4 = new Woman("Suzan", "Valera", "10/01/1985", 90, motherScedule);
        Man father4 = new Man("Max", "Valera", "29/07/1975", 95, fatherScedule);

        Human child = new Man("Valera", "Valerich", "16/02/1998", 95, childScedule);
        Human child2 = new Woman("Valerian", "Surnamich", "10/08/1999", 95, childScedule);
        Human child3 = new Man("Alex", "Alexich", "11/07/2002", 95, childScedule);
        Human child4 = new Man("Deny", "Denich", "08/07/2008", 95, childScedule);

        FamilyDao dataBase = new CollectionFamilyDao();
        FamilyService service = new FamilyService(dataBase);
        FamilyController controller = new FamilyController(service);


        loop:
        while (true){
            String index,count;
            generateCommands();
            String answer = in.nextLine();
            switch (answer){
                case "1" :
                    controller.createNewFamily(mother,father);
                    controller.createNewFamily(mother2,father2);
                    continue loop;
                case "2":
                    controller.displayAllFamilies();
                    continue loop;
                case "3":
                    System.out.println("Enter a  number");
                    count = in.nextLine();
                    controller.getFamiliesBiggerThan(Integer.parseInt(count));
                    continue loop;
                case "4":
                    System.out.println("Enter a  number");
                    count = in.nextLine();
                    controller.getFamiliesLessThan(Integer.parseInt(count));
                    continue loop;
                case "5":
                    System.out.println("Enter a  number");
                    count = in.nextLine();
                    System.out.println(controller.countFamiliesWithMemberNumber(Integer.parseInt(count)));
                    continue loop;
                case "6":
                    controller.createNewFamily(generateHuman(true,false),generateHuman(false,false));
                    continue loop;

                case "7":
                    System.out.println("Enter index of family(starting from 1)");
                    index = in.nextLine();
                    controller.deleteFamilyByIndex(Integer.parseInt(index)-1);
                    continue loop;

                case "8":
                        generateForEditingCase();
                        String answerForEditing = in.nextLine();

                        switch (answerForEditing){
                            case "1":
                                System.out.println("Enter an id of family(starting from 1)");
                                index = in.nextLine();

                                System.out.println("Enter boy`s name");
                                String boyName = in.nextLine();

                                System.out.println("Enter girl`s name");
                                String girlName = in.nextLine();
                                controller.bornChild(controller.getFamilyById(Integer.parseInt(index)-1),boyName,girlName);
                                continue loop;
                            case "2":
                                Human child5;
                                System.out.println("Enter an id of family(starting from 1)");
                                index = in.nextLine();

                                System.out.println("Enter gender(M - man, W - woman)");
                                String gender = in.nextLine();
                                boolean gen = gender.toLowerCase().trim().equals("m") ? false : true;
                                controller.adoptChild(controller.getFamilyById(Integer.parseInt(index)-1),generateHuman(gen,true));
                                continue loop;
                            case "3":
                                continue loop;
                            default :
                                System.out.println("Invalid input");
                                continue loop;
                        }

                case "9":
                    System.out.println("Enter age");
                    String age = in.nextLine();
                    controller.deleteAllChildrenOlderThen(Integer.parseInt(age));
                    continue loop;

                case "exit" :
                    break loop;

                default:
                    System.out.println("Invalid input. Enter once more");
                    continue loop;
            }
        }

    }


    public static Human generateHuman(boolean gender,boolean child){
//        gender = true - Woman
        System.out.println(!child ? gender ? "Enter mother's name" :  "Enter father's name" : "Enter name");
        String name = in.nextLine();
        System.out.println(!child ? gender ? "Enter mother's surname" :  "Enter father's surname" : "Enter surname");
        String surname = in.nextLine();
        System.out.println(!child ? gender ? "Enter mother's year(yyyy)" :  "Enter father's year(yyyy)" : "Enter year(yyyy)");
        String year = in.nextLine();
        System.out.println(!child ? gender ? "Enter mother's month(mm)" :  "Enter father's month(mm)" : "Enter month(mm)");
        String month = in.nextLine();
        System.out.println(!child ? gender ? "Enter mother's day(dd)" :  "Enter father's day(dd)" : "Enter day(dd)");
        String day = in.nextLine();
        String concatBirthday = day + "/" + month + "/" + year;
        System.out.println(!child ? gender ? "Enter mother's iq" :  "Enter father's iq" : "Enter iq");
        String iq = in.nextLine();
        Human newHuman = gender ? new Woman(name,surname,Integer.parseInt(iq),concatBirthday) : new Man(name,surname,Integer.parseInt(iq),concatBirthday);
        return newHuman;
    }

    public static void generateForEditingCase(){
        String output = "1. Born child\n"+
                        "2. Adopt child\n"+
                        "3. Back to the menu";
        System.out.println(output);
    }

    public static void generateCommands(){
        String output = "1. Fill with test data\n" +
                "2. Display all families\n" +
                "3. Display families where the number of people is more than the specified\n" +
                "4. Display families where the number of people is less than the specified\n" +
                "5. Count families where number of people is equals to specified\n" +
                "6. Create new family\n" +
                "7. Delete family by index\n" +
                "8. Edit family by index\n" +
                "9. Delete all children older than";
        System.out.println(output);
    }
}
