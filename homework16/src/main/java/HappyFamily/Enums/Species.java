package HappyFamily.Enums;

public enum Species {
    CAT ("Cat",false,4,true),
    DOG ("Dog",false,4,true),
    SNAKE ("Snake",false,0,false),
    RABBIT ("Rabbit",false,4,true),
    SPIDER ("Spider",false,8,true),
    OWL ("Owl",true,2,false),
    FISH("Fish",false,0,false),
    DOMESTICCAT("DomesticCat",false,4,true),
    ROBOCAT("RoboCat",false,4,false),
    UNKNOWN("Unknown",false,0,false);

    private String description;
    private boolean canFly;
    private int numberOfLegth;
    private boolean hasFur;


    Species(String description , boolean canFly, int numberOfLegth, boolean hasFur){
        this.description = description;
        this.canFly = canFly;
        this.numberOfLegth = numberOfLegth;
        this.hasFur = hasFur;
    }



    public String getDescription() {
        return description;
    }

    public boolean isCanFly() {
        return canFly;
    }

    public int getNumberOfLegth() {
        return numberOfLegth;
    }

    public boolean isHasFur() {
        return hasFur;
    }
}
