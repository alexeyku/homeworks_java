package HappyFamily.Pets;

import HappyFamily.Enums.Species;


import java.util.Objects;
import java.util.Set;

public class RoboCat extends Pet implements Foul{

    public RoboCat(String nickname, int age, int trickLevel, Set<String> habits){
        super(nickname,age,trickLevel,habits);
        species = Species.ROBOCAT;
    }

    public String foul(){
        return "Нужно хорошо замести следы...";
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RoboCat robocat = (RoboCat) o;
        return  Objects.equals(species, robocat.species) &&
                Objects.equals(nickname, robocat.nickname);
    }

    public int hashCode() {
        return species.hashCode() + nickname.hashCode() + age * 31;
    }

    @Override
    public String respond(){
        return "Привет, хозяин. Я - " + nickname + ". Я соскучился!";
    }

}
