package HappyFamily.People;
import HappyFamily.Enums.*;
import org.junit.Test;

import java.util.*;


import static org.junit.Assert.*;

public class FamilyTest {
    Set<String> arr = new HashSet<String>();

    Map<String,String> motherScedule = new HashMap<String,String>();

    Map<String,String> fatherScedule = new HashMap<String,String>();


    Map<String,String> childScedule = new HashMap<String,String>();

    Human mother = new Human("Mary", "Winchester","10/10/1974",90,motherScedule);

    Human father = new Human("John", "Winchester","10/10/1974",95,fatherScedule);

    Family family = new Family(mother,father);

    Human child = new Human("Sam", "Winchester","10/10/1974",85 ,childScedule);
    Human child2 = new Human("Dean", "Winchester","10/10/1974",80 ,childScedule);
    Human child3 = new Human("Trevor", "Winchester","10/10/1974",75 ,childScedule);
    Human child4 = new Human("Bobby", "Winchester","10/10/1974",45 ,childScedule);
    Human child5 = new Human("Kostya", "Winchester","10/10/1974",10 ,childScedule);
    Human mother2 = new Human("Jane", "Winch","10/10/1974",90,motherScedule);
    Human father2 = new Human("Bobby", "Winch","10/10/1974",95,fatherScedule);

    Family family3 = new Family(mother2,father2);


    protected void setUp() throws Exception{

        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");

        fatherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(),"visit parents");

        childScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(),"walk with mother");
    }


    @Test
    public void bornChild() {
        Human child6 = family.bornChild();
        assertTrue(child6 instanceof Man || child6 instanceof Woman);
        assertTrue(child6.getFamily() == father.getFamily() && child6.getFamily() == mother.getFamily());
        assertTrue(child6.getSurname() == father.getSurname());
        assertNotNull(child6.getName());
    }

    @Test
    public void testHashCode(){
        family3 = family;
        if(family.equals(family3)){
            assertTrue(family.hashCode() == family3.hashCode());
        }
    }


    @Test
    public void testEquals(){
        //        1. obj == obj2
        Family family2 = family;
        assertTrue(family.equals(family2));
        assertTrue(family2.equals(family));

//        2.

//            2.1 obj2 == null
        family2 = null;
        assertFalse(family.equals(family2));

//            2.2 obj.getClass() != obj2.getClass()
//        Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);
//        assertFalse(family.equals(dog));

//        3. fields comparison
        assertFalse(family.equals(family2));

        family2 = new Family(mother,father);
        assertTrue(family.equals(family2));
    }


    @Test
    public void testToString() {
        assertEquals(getExpextedToString(family),family.toString());
        assertEquals(getExpextedToString(family3),family3.toString());
    }


    @Test
    public void testAddChild() {
        family.addChild(child);
        int initLength = family.getChildren().size();
        family.addChild(child2);

        assertSame(initLength+1, family.getChildren().size());
        assertSame(family.getChildren().get(family.getChildren().size()-1),child2);
    }


    @Test
    public void testDeleteChild() {
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);

        family.deleteChild(child2);
        for (Human child : family.getChildren()){
            assertNotSame(child,child2);
        }

//            doesn`t exist
        family.deleteChild(child5);

        for (Human child : family.getChildren()){
            assertNotSame(child,child5);
        }

    }


    @Test
    public void testDeleteChildByIndex() {
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);

//        to check 1st 2 elements before deleting
        assertSame(child,family.getChildren().get(0));
        assertSame(child2,family.getChildren().get(1));

        assertTrue(family.deleteChild(0));
        assertSame(child2,family.getChildren().get(0));

        int childsLength = family.getChildren().size();
        assertFalse(family.deleteChild(7));
        assertTrue(family.getChildren().size() == childsLength);
    }
//


    @Test
    public void countFamily() {
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        int expextedLength = family.getChildren().size() + 2;
        assertEquals(expextedLength,family.countFamily());
    }



    public String getExpextedToString(Family target){
        String output = "Family{" +
                " \n mother= " + target.getMother() +
                ",\n father= " + target.getFather() +
                ",\n chiildren= " + target.getChildren() +
                ",\n pet=" + target.getPets().toString() +
                '}';
        return output;
    }
}