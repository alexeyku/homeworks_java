package HappyFamily.People;

import HappyFamily.DayOfWeek;
import java.util.HashMap;
import java.util.Map;

import HappyFamily.Pets.*;

final public class Woman extends Human{

    public Woman(String name, String surname, int year , int iq, Map<String,String> scedule){
        super(name,surname,year,iq,scedule);
    }

    public Woman(String name, String surname, int iq, int year, Family family){
        super(name,surname,iq,year,family);
    }



    @Override
    public String greetPet(){
        if(family == null) return null;
        String output = "";
        if(family.getPets() != null){
            for(Pet pet : family.getPets()){
                output+= "Привееет, " + pet.getNickname() + "))) \n";
            }

        }
        else output =  "Нету животного, нужно завести...";
        return output;
    }

    public String makeup(){
        return "Через 2 минуты буду готова, честное слово";
    }
}
