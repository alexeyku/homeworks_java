package HappyFamily;

import HappyFamily.People.*;
import HappyFamily.Pets.*;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

        Map<String,String> motherScedule = new HashMap<String,String>();
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");

        Map<String,String> fatherScedule = new HashMap<String,String>();
        fatherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(),"visit parents");

        Map<String,String> childScedule = new HashMap<String,String>();
        childScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(),"walk with mother");


        RoboCat cat = new RoboCat("Murka",5,55,arr);
        Dog dog = new Dog("Bobik",1,33,arr);

        Woman mother = new Woman("Mary", "Winchester",1977,90,motherScedule);

        Man father = new Man("John", "Winchester",1974,95,fatherScedule);

        Family family = new Family(mother,father);

        family.addPet(cat);
        family.addPet(dog);

        System.out.println(father.feedPet(false));
        System.out.println(mother.greetPet());
        Human child = family.bornChild();
        child.setScedule(childScedule);
        Human child2 = family.bornChild();
        child2.setScedule(childScedule);

        System.out.println(child.getFamily());
        System.out.println(mother.makeup());
        System.out.println(father.repairCar());

    }

}
