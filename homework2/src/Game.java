import java.util.Scanner;
import java.util.Arrays;
import java.util.Random;

public class Game {
    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        String regex = "[1-5]";
        Random ran = new Random();

        int targetsCounter = 3;

//        random for rows
        int random = 1 + ran.nextInt(5);
//        random for columns
        int random2 = 1 + ran.nextInt(5);
//        random for target position(horizontal or vertical)
        int random3 = 1 + ran.nextInt(2) ;

//        targets
        int[] target1 = new int[2];
        int[] target2 = new int[2];
        int[] target3 = new int[2];

//        field
        String[][] field = new String[6][6];
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field.length; j++) {
                if(i==0){
                    field[i][j] = j + " |";
                }
                else{
                    if(j == 0){
                        field[i][0] = i + " |";
                    }
                    else{
                        field[i][j] ="- |";
                    }
                }
            }
        }

        fieldInit(field);

//       creating targets on the field
        if(random3 == 1){

            if(field[random-1][random2] != null && field[random-1][random2] == "- |"){

                target1[0] = random;
                target1[1] = random2;

                target2[0] = random-1;
                target2[1] = random2;

                if(random+1 < field.length-1){

                    target3[0] = random+1;
                    target3[1] = random2;
                }
                else{

                    target3[0] = random-2;
                    target3[1] = random2;
                }

            }
            else {

                target1[0] = random;
                target1[1] = random2;

                target2[0] = random+1;
                target2[1] = random2;

                target3[0] = random+2;
                target3[1] = random2;
            }

        }
        else{

            if(field[random][random2-1] != null && field[random][random2-1] == "- |"){
                target1[0] = random;
                target1[1] = random2;

                target2[0] = random;
                target2[1] = random2-1;
                if(random2+1 < field.length-1){
                    target3[0] = random;
                    target3[1] = random2+1;
                }
                else{
                    target3[0] = random;
                    target3[1] = random2-2;
                }
            }
            else {

                target1[0] = random;
                target1[1] = random2;

                target2[0] = random;
                target2[1] = random2+1;

                target3[0] = random;
                target3[1] = random2+2;
            }

        }

        System.out.println("All set. Get ready to rumble!");
//        game
        game:
        for (; ; ) {
            System.out.println("Enter the row :");
            String rowStr = in.nextLine();
            if (rowStr.matches(regex)) {
                int row = Integer.parseInt(rowStr);
                for (;;) {
                    System.out.println("Enter the column :");
                    String columnStr = in.nextLine();
                    if (columnStr.matches(regex)) {
                        int column = Integer.parseInt(columnStr);

                        if(row == target1[0] && column == target1[1]){
                            field[row][column] = "x |";
                            targetsCounter--;
                                if(checkWon(targetsCounter,field)){
                                    break game;
                                }

                        }
                        else if(row == target2[0] && column == target2[1]){
                            field[row][column] = "x |";
                            targetsCounter--;
                                if(checkWon(targetsCounter,field)){
                                    break game;
                                }
                        }
                        else if(row == target3[0] && column == target3[1]){
                            field[row][column] = "x |";
                            targetsCounter--;
                                if(checkWon(targetsCounter,field)){
                                    break game;
                                }

                        }
                        else{
                            field[row][column] = "* |";
                        }
                        fieldInit(field);
                        continue game;
                    }
                    else {
                        System.out.println("Incorrect value");
                    }
                }
            }
            else {
                System.out.println("Incorrect value");
            }
        }
    }

    public static void fieldInit(String[][] field){
        for (int i = 0; i < field.length; i++) {
            for (int j = 0; j < field[i].length; j++) {
                System.out.print(field[i][j] + "\t");
            }
            System.out.println();
        }
    }

    public static boolean checkWon(int counter , String[][] field){
        if(counter == 0){
            fieldInit(field);
            System.out.println("You have won!");
            return true;
        }
        return false;
    }
}
