package HappyFamily;


public class Main {
    public static void main(String[] args) {
        String[] arr = {"eat, drink, sleep"};
        String[][] motherScedule = {
                {"Saturday","relax with family"},
                {"Sunday","walk with child"},
        };
        String[][] fatherScedule = {
                {"Saturday","relax with family"},
                {"Sunday","visit parents"}
        };

        String[][] childScedule = {
                {"Saturday","relax with family"},
                {"Sunday","walk with mother"}
        };



//        family 1
        Pet dog = new Pet("dog","Bobik",7,51,arr);

        Human mother = new Human("Jane", "Karleone",1977,90,dog,new Human("Judi","Smith"),new Human("Jake","Karleone"),motherScedule);

        Human father = new Human("Vito", "Karleone",1974,95,dog,new Human("Juli","Karleone"),new Human("Alex","Karleone"),fatherScedule);

        Human child = new Human("Michael", "Karleone",2007,85,dog,mother,father,childScedule);

        System.out.println(child.toString());
        System.out.println(mother.toString());
        System.out.println(father.toString());
        System.out.println("");
        System.out.println("");
        System.out.println("");

//        family 2
        Pet cat = new Pet("cat","Murka");

        Human motherSecond = new Human ("Mary", "Winchester",1954);

        Human fatherSecond = new Human("John","Winchester", 1954);

        Human childSecond = new Human("Sam","Winchester",1983);

        System.out.println("Second family");
        System.out.println(motherSecond.toString());
        System.out.println(fatherSecond.toString());
        System.out.println(childSecond.toString());
        System.out.println("");
        System.out.println("");
        System.out.println("");


//        family 3
        Pet dog2 = new Pet();

        Human motherThird = new Human("Alexis","Williams",1990, new Human("Mia","Miller"),new Human("Mason","Miller"));

        Human fatherThird = new Human("Liam","Williams",1989, new Human("Olivia","Williams"), new Human("James","Williams"));

        Human childThird = new Human("Jake","Williams",2009,motherThird,fatherThird);


        System.out.println("Third family");
        System.out.println(motherThird.toString());
        System.out.println(fatherThird.toString());
        System.out.println(childThird.toString());

        System.out.println("");
        System.out.println("");
        System.out.println("");




        //    family 4

        Human motherFourth = new Human();
        Human fatherFourth = new Human();
        Human childFourth = new Human();

        System.out.println("Fourth family");
        System.out.println(motherFourth.toString());
        System.out.println(fatherFourth.toString());
        System.out.println(childFourth.toString());

    }





}
