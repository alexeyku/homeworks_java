package HappyFamily;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class FamilyTest {

    String[] arr = {"eat, drink, sleep"};
    String[][] childScedule = {
            {DayOfWeek.SATURDAY.name(),"relax with family"},
            {DayOfWeek.SUNDAY.name(),"walk with mother"}
    };

    String[][] motherScedule = {
            {DayOfWeek.SATURDAY.name(),"relax with family"},
            {DayOfWeek.SUNDAY.name(),"walk with child"},
    };
    String[][] fatherScedule = {
            {DayOfWeek.SATURDAY.name(),"relax with family"},
            {DayOfWeek.SUNDAY.name(),"visit parents"}
    };

    Human mother = new Human("Mary", "Winchester",1977,90,motherScedule);

    Human father = new Human("John", "Winchester",1974,95,fatherScedule);

    Family family = new Family(mother,father);

    Human child = new Human("Sam", "Winchester",2007,85 ,childScedule);
    Human child2 = new Human("Dean", "Winchester",2000,80 ,childScedule);
    Human child3 = new Human("Trevor", "Winchester",1998,75 ,childScedule);
    Human child4 = new Human("Bobby", "Winchester",1956,45 ,childScedule);
    Human child5 = new Human("Kostya", "Winchester",1748,10 ,childScedule);
    Human mother2 = new Human("Jane", "Winch",2000,90,motherScedule);
    Human father2 = new Human("Bobby", "Winch",2005,95,fatherScedule);

    Family family3 = new Family(mother2,father2);


    @Test
    public void testHashCode(){
        family3 = family;
        if(family.equals(family3)){
            assertTrue(family.hashCode() == family3.hashCode());
        }
    }


    @Test
    public void testEquals(){
        //        1. obj == obj2
        Family family2 = family;
        assertTrue(family.equals(family2));
        assertTrue(family2.equals(family));

//        2.

//            2.1 obj2 == null
        family2 = null;
        assertFalse(family.equals(family2));

//            2.2 obj.getClass() != obj2.getClass()
        Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);
        assertFalse(family.equals(dog));

//        3. fields comparison
        assertFalse(family.equals(family2));

        family2 = new Family(mother,father);
        assertTrue(family.equals(family2));
    }


    @Test
    public void testToString() {
        assertEquals(getExpextedToString(family),family.toString());
        assertEquals(getExpextedToString(family3),family3.toString());
    }


    @Test
    public void testAddChild() {
        family.addChild(child);
        int initLength = family.getChildren().length;
        family.addChild(child2);

        assertSame(initLength+1, family.getChildren().length);
        assertSame(family.getChildren()[family.getChildren().length-1],child2);
    }


    @Test
    public void testDeleteChild() {
            family.addChild(child);
            family.addChild(child2);
            family.addChild(child3);
            family.addChild(child4);

            family.deleteChild(child2);
            for (int i = 0; i < family.getChildren().length; i++) {
                assertNotSame(child2,family.getChildren()[i]);
            }

//            doesn`t exist
            family.deleteChild(child5);

            for (int i = 0; i < family.getChildren().length; i++) {
                assertNotSame(child5,family.getChildren()[i]);
            }
    }


    @Test
    public void testDeleteChildByIndex() {
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        family.addChild(child4);

//        to check 1st 2 elements before deleting
        assertSame(child,family.getChildren()[0]);
        assertSame(child2,family.getChildren()[1]);

        assertTrue(family.deleteChild(0));
        assertSame(child2,family.getChildren()[0]);

        int childsLength = family.getChildren().length;
        assertFalse(family.deleteChild(7));
        assertTrue(family.getChildren().length == childsLength);
    }
//


    @Test
    public void countFamily() {
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child3);
        int expextedLength = family.getChildren().length + 2;
        assertEquals(expextedLength,family.countFamily());
    }



    public String getExpextedToString(Family target){
        String output = "Family{" +
                " \n mother= " + target.getMother() +
                ",\n father= " + target.getFather() +
                ",\n chiildren= " + Arrays.toString(target.getChildren()) +
                ",\n pet=" + target.getPet() +
                '}';
        return output;
    }
}