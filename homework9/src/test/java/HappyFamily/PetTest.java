package HappyFamily;

import org.junit.Test;

import java.util.Arrays;

import static org.junit.Assert.*;

public class PetTest {
    String[] arr = {"eat, drink, sleep"};

    Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);
    Pet cat = new Pet(Species.DOG,"Murka",5,21,arr);

    Pet dog2 = dog;

    @Test
    public void testToString() {
        assertEquals(getExpextedToString(dog),dog.toString());
        assertEquals(getExpextedToString(cat),cat.toString());
    }

    @Test
    public void testEquals(){
        //        1. obj == obj2
        assertTrue(dog.equals(dog2));
        assertTrue(dog2.equals(dog));

//        2.

//            2.1 obj2 == null
        dog2 = null;
        assertFalse(dog.equals(dog2));

//            2.2 obj.getClass() != obj2.getClass()

        assertFalse(dog.equals(cat));

//        3. fields comparison
        dog2 = new Pet(Species.DOG,"NotBobik",7,51,arr);
        assertFalse(dog.equals(dog2));

        dog2 = new Pet(Species.DOG,"Bobik",7,51,arr);
        assertTrue(dog.equals(dog2));
    }

    @Test
    public void testHashCode(){
        if(dog.equals(dog2)){
            assertTrue(dog.hashCode() == dog2.hashCode());
        }
    }


    public String getExpextedToString(Pet target){
        String output = target.getSpecies().getDescription() + "{\n\tnickname = '" + target.getNickname() + "', age = " + target.getAge() + ",trickLevel = " + target.getTrickLevel() + ", can fly = " + target.getSpecies().isCanFly() + ", number of legth = " + target.getSpecies().getNumberOfLegth() + ", has fur = " + target.getSpecies().isHasFur() + ", habits = " + Arrays.toString(target.getHabits()) + "\n\t}";
        return output;
    }
}