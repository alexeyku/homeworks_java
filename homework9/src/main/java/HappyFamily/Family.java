package HappyFamily;


import java.util.Arrays;
import java.util.Objects;

public class Family {
    private Human mother;
    private Human father;
    private Human[] children;
    private Pet pet;


    static {
        System.out.println("Family initialised");
    }

    {
        System.out.println("New Family`s object is creating");
    }


    public Family(Human mother, Human father) {
        this.mother = mother;
        this.father = father;
        this.children = null;
        this.mother.setFamily(this);
        this.father.setFamily(this);
    }


//    methods


    protected void finalize () {
        System.out.println (this.toString() + " is deleting");
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Family family = (Family) o;
        return Objects.equals(mother, family.mother) &&
                Objects.equals(father, family.father);
    }

    @Override
    public int hashCode() {
        int result = mother.hashCode() + father.hashCode() * 31;
        return result;
    }

    @Override
    public String toString() {
        return "Family{" +
                " \n mother= " + mother +
                ",\n father= " + father +
                ",\n chiildren= " + Arrays.toString(children) +
                ",\n pet=" + pet +
                '}';
    }

    public void addChild(Human child){
        child.setFamily(this);
        if(children != null && Arrays.asList(children).indexOf(child) == -1){
            children = Arrays.copyOf(children,children.length+1);
            children[children.length-1] = child;
        }
        else if(children == null){
            children = new Human[1];
            children[0] = child;
        }
    }

    public boolean deleteChild(int index){
        if(isNotEmpty() && index <= children.length-1){
            Human[] newArr = new Human[children.length-1];
            int j = 0;
            for (int i = 0; i < children.length; i++) {
                if(i == index) continue;
                newArr[j] = children[i];
                j++;
            }
            children = newArr;
            return true;
        }
        else return false;
    }

    public void deleteChild(Human child){
        if(isNotEmpty() && Arrays.asList(children).indexOf(child) != -1){
            Human[] newArr = new Human[children.length-1];
            int j = 0;
            for (int i = 0; i < children.length; i++) {
                if(child.hashCode() == children[i].hashCode()){
                    if(child.equals(children[i])) continue;
                }
                else{
                    newArr[j] = children[i];
                    j++;
                }

            }
            children = newArr;
        }
    }

    public boolean isNotEmpty(){
        return children != null && children.length != 0;
    }


    public int countFamily(){
        int result = 2;
        if(children == null) return result;
        result += children.length;
        return result;
    }


    //    getters/setters
    public Human getMother() {
        return mother;
    }

    public Human getFather() {
        return father;
    }

    public Human[] getChildren() {
        return children;
    }

    public Pet getPet() {
        return pet;
    }

    public void setPet(Pet pet) {
       this.pet = pet;
    }


    public void setMother(Human mother) {
        this.mother = mother;
    }

    public void setFather(Human father) {
        this.father = father;
    }

    public void setChiildren(Human[] children) {
        this.children = children;
    }
}
