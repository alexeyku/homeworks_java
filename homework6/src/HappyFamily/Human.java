package HappyFamily;

import java.util.Arrays;
import java.util.Objects;
import java.util.Random;

public class Human {
    private String name;
    private String surname;
    private int year;
    private int iq;
    private Family family;
    private String[][] scedule;

    public Human(String name, String surname, int year , int iq, String[][] scedule){
        this.name = name;
        this.surname = surname;
        this.year = year;
        this.iq = iq;
        this.scedule = scedule;
    }

//    methods


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Human human = (Human) o;
        return year == human.year &&
                Objects.equals(name, human.name) &&
                Objects.equals(surname, human.surname) &&
                Objects.equals(family, human.family);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name, surname, year, family);
    }



    public String greetPet(){
        if(this.getFamily().getPet() != null){
            return "Привет, " + this.getFamily().getPet().getNickname();
        }
        else return "Надо завести животное , чтобы его поприветствовать";
    }

    public String describePet(){
        if(this.getFamily().getPet() != null){
            String trick = "";
            if(this.getFamily().getPet().getTrickLevel() > 50 ){
                trick = "очень хитрый";
            }
            else{
                trick = "почти не хитрый";
            }
            String output = "У меня есть " + this.getFamily().getPet().getSpecies() + " , ему " + this.getFamily().getPet().getAge() + " лет, он " + trick;
            return output;
        }
        else return "Надо завести животное , чтобы его можно было отписать";

    }

    public boolean feedPet(boolean flag){
        if(this.getFamily().getPet() != null){
            String output = "";
            boolean result = false;
            if(flag){
                output = "Хм... покормлю ка я " + this.getFamily().getPet().getNickname();
                result = true;
            }
            else{
                Random ran = new Random();
                int random = ran.nextInt(101);
                if(this.getFamily().getPet().getTrickLevel() > random){
                    output = "Хм... покормлю ка я " + this.getFamily().getPet().getNickname();
                    result = true;
                }
                else{
                    output = "Думаю, " + this.getFamily().getPet().getNickname() + " не голоден.";
                }
            }
            System.out.println(output);
            return result;
        }
        else{
            System.out.println("Надо завести животное , чтобы его покормить");
            return false;
        }
    }



    public String toString(){
        String output = "\n" + this.getClass().getSimpleName() + "{\n name = " + this.name + ",\n surname = " + this.surname + ",\n year = " + this.year + ",\n iq = " + this.iq + ",\n scedule = " + Arrays.deepToString(this.scedule)  + "\n}";
        return output;
    }

    public void setFamily(Family family) {
        this.family = family;
    }

    public Family getFamily() {
        return this.family;
    }

    public String[][] getScedule() {
        return this.scedule;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSurname() {
        return surname;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public int getYear() {
        return year;
    }

    public void setYear(int year) {
        this.year = year;
    }

    public int getIq() {
        return iq;
    }

    public void setIq(int iq) {
        this.iq = iq;
    }

    public void setScedule(String[][] scedule) {
        this.scedule = scedule;
    }
}
