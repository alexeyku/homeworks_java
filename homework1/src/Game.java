import java.util.Scanner;
import java.util.Random;
import java.util.Arrays;

public class Game {
    private static int[] addEllement(int[] arr, int elem){
        arr = Arrays.copyOf(arr,arr.length+1);
        arr[arr.length-1] = elem;
        return arr;
    }
    private static int[] mySort(int[] arr){
        int[] output = new int[arr.length];
        int j = 0;
        Arrays.sort(arr);
        for (int i = arr.length-1; i >= 0; i--) {
            output[j] = arr[i];
            j++;
        }
        return output;
    }
    public static void main(String[] args) {
        int[] answers = {};
    	String array[][] = new String[7][7];
    	array[0][0] = "What is the year now";
    	array[1][0] = "2018";
    	array[0][1] = "When did the World War II begin?";
    	array[1][1] = "1939";
    	array[0][2] = "When did the World War II end?";
    	array[1][2] = "1945";
    	array[0][3] = "When did the World War I begin?";
    	array[1][3] = "1914";
    	array[0][4] = "When did the World War I end?";
    	array[1][4] = "1918";
    	array[0][5] = "The year of Brest Union";
    	array[1][5] = "1596";
    	array[0][6] = "The year of Lublin Union";
    	array[1][6] = "1569";
        Scanner in = new Scanner(System.in);
        Random ran = new Random();
        String regex = "[0-9]+";
        System.out.println("Enter your name");
        String name = in.nextLine();
        System.out.println("Let the game begin!");
        int random = ran.nextInt(7);
        System.out.println(array[0][random]);
        for (;;) {
        	System.out.println("Enter your answer");
            String answer = in.nextLine();
            if(answer.matches(regex)){
                int ans = Integer.parseInt(answer);
                int res = Integer.parseInt(array[1][random]);
                if(ans<res){
                    answers = addEllement(answers,ans);
                    System.out.println("Your number is too small. Please, try again.");
                }
	            else if(ans>res){
                    answers = addEllement(answers,ans);
	            	System.out.println("Your number is too big. Please, try again.");
	            }
                else{
                    answers = addEllement(answers,ans);
                    System.out.println("Consgratulations, " + name);
                    System.out.println("Your numbers: " + Arrays.toString(mySort(answers)));
                    break;
                }
            }
            else{
                System.out.println("Incorrect input");
            }
        }
    }
}