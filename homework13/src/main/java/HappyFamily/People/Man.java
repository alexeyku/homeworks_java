package HappyFamily.People;

import HappyFamily.DayOfWeek;
import HappyFamily.Pets.Pet;

import java.util.HashMap;
import java.util.Map;

final public class Man extends Human{

    public Man(String name, String surname, int year , int iq, Map<String,String> scedule){
        super(name,surname,year,iq,scedule);
    }

    public Man(String name, String surname, int iq, int year, Family family){
        super(name,surname,iq,year,family);
    }

    @Override
    public String greetPet(){
        if(family == null) return null;
        String output = "";
        if(family.getPets() != null){
            for(Pet pet : family.getPets()){
                output+= "Привееет, " + pet.getNickname() + "))) \n";
            }

        }
        else  output = "Нету животного";
        return output;
    }

    public String repairCar(){
        return "Пойду-ка я чинить машину" ;
    }

}
