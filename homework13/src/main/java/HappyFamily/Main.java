package HappyFamily;

import HappyFamily.Controller.FamilyController;
import HappyFamily.People.*;
import HappyFamily.Pets.*;
import HappyFamily.DAO.*;
import HappyFamily.Service.FamilyService;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Main {
    public static void main(String[] args) {
        Set<String> arr = new HashSet<String>();
        arr.add("eat");
        arr.add("sleep");
        arr.add("drink");

        Map<String,String> motherScedule = new HashMap<String,String>();
        motherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        motherScedule.put(DayOfWeek.SUNDAY.name(),"walk with child");

        Map<String,String> fatherScedule = new HashMap<String,String>();
        fatherScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        fatherScedule.put(DayOfWeek.SUNDAY.name(),"visit parents");

        Map<String,String> childScedule = new HashMap<String,String>();
        childScedule.put(DayOfWeek.SATURDAY.name(),"relax with family");
        childScedule.put(DayOfWeek.SUNDAY.name(),"walk with mother");


        RoboCat cat = new RoboCat("Murka",5,55,arr);
        Dog dog = new Dog("Bobik",1,33,arr);

        Woman mother = new Woman("Mary", "Winchester",1977,90,motherScedule);

        Man father = new Man("John", "Winchester",1974,95,fatherScedule);

        Woman mother2 = new Woman("Jane", "Smith",1977,90,motherScedule);

        Man father2 = new Man("Bred", "Smith",1974,95,fatherScedule);


        Human child = new Man("Name", "Surname",1998,95,childScedule);
        Human child2 = new Man("Name2", "Surname",1999,95,childScedule);
        Human child3 = new Man("Name3", "Surname",2002,95,childScedule);


        FamilyDao dataBase = new CollectionFamilyDao();

        FamilyService service = new FamilyService(dataBase);

        FamilyController controller = new FamilyController(service);
        controller.createNewFamily(mother,father);
        controller.createNewFamily(mother2,father2);
        controller.adoptChild(controller.getFamilyById(0),child);
        controller.adoptChild(controller.getFamilyById(0),child2);
        controller.adoptChild(controller.getFamilyById(0),child3);
        controller.bornChild(controller.getFamilyById(1),"Dany","Kate");
        controller.addPet(0,dog);
        controller.addPet(0,cat);
        controller.deleteAllChildrenOlderThen(19);
        controller.getFamiliesLessThan(3);
        controller.getFamiliesBiggerThan(2);
        System.out.println(controller.countFamiliesWithMemberNumber(3));
        System.out.println(controller.getPets(0));
    }

}
