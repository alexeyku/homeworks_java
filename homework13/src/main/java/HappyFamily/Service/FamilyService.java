package HappyFamily.Service;

import HappyFamily.DAO.FamilyDao;
import HappyFamily.People.*;
import HappyFamily.Pets.Pet;

import java.sql.SQLOutput;
import java.util.*;

public class FamilyService {
    private FamilyDao familyDao;

    public FamilyService(FamilyDao familyDao) {
        this.familyDao = familyDao;
    }

    public List<Family> getAllFamilies(){
        return familyDao.getAllFamilies();
    }

    public void displayAllFamilies(){
        for (int i = 0; i < familyDao.getAllFamilies().size(); i++) {
            System.out.println( i+1 + ":\t" + familyDao.getFamilyByIndex(i));
        }
    }


    public List<Family> getFamiliesLessThan (int number){
        List<Family> result = new ArrayList<>();
        for(Family family : familyDao.getAllFamilies()){
            if(family.countFamily() < number){
                result.add(family);
                System.out.println(family);
            }
        }
        return result;
    }

    public List<Family> getFamiliesBiggerThan(int number){
        List<Family> result = new ArrayList<>();
        for(Family family : familyDao.getAllFamilies()){
            if(family.countFamily() > number){
                result.add(family);
                System.out.println(family);
            }
        }
        return result;
    }


    public int countFamiliesWithMemberNumber (int number){
        int result = 0;
        for(Family family : familyDao.getAllFamilies()){
            if(family.countFamily() == number){
                result++;
            }
        }
        return result;
    }



    public void createNewFamily(Human mother,Human father){
        Family family = new Family(mother,father);
        familyDao.saveFamily(family);
    }


    public void deleteFamilyByIndex(int index){
        familyDao.deleteFamily(index);
    }


    public Family bornChild(Family family , String boyName , String girlName){
        Human child = family.bornChild();
        if(child instanceof Man){
            child.setName(boyName);
        }
        else{
            child.setName(girlName);
        }
        return family;
    }


    public Family adoptChild(Family family,Human child){
        family.addChild(child);
        return family;
    }


    public void deleteAllChildrenOlderThen(int age){
        Date date = new Date();
        Calendar cal = Calendar.getInstance();
        cal.setTime(date);
        int nowYear = cal.get(Calendar.YEAR);

        for(Family family : familyDao.getAllFamilies()){
            Iterator<Human> i = family.getChildren().iterator();
            while(i.hasNext()){
                Human child = i.next();
                int year = nowYear - child.getYear();
                if(year > age){
                    i.remove();
                }
            }
        }
    }


    public int count(){
        return familyDao.getAllFamilies().size();
    }

    public Family getFamilyById(int index){
        if(!validateIndex(index)) return null;
        return familyDao.getAllFamilies().get(index);
    }


    public Set<Pet> getPets(int index){
        if(!validateIndex(index)) return null;
        return familyDao.getAllFamilies().get(index).getPets();
    }

    public void addPet(int index,Pet pet){
        if(validateIndex(index)){
            familyDao.getAllFamilies().get(index).addPet(pet);
        }
    }


    public boolean validateIndex(int ind){
        return ind < familyDao.getAllFamilies().size() && ind >= 0;
    }

    public FamilyDao getFamilyDao() {
        return familyDao;
    }

}
