package HappyFamily.Controller;

import HappyFamily.People.Family;
import HappyFamily.People.Human;
import HappyFamily.Pets.Pet;
import HappyFamily.Service.FamilyService;

import java.util.*;

public class FamilyController {
    private FamilyService familyService;

    public FamilyController(FamilyService familyService) {
        this.familyService = familyService;
    }

    public List<Family> getAllFamilies(){
        return  familyService.getAllFamilies();
    }

    public void displayAllFamilies(){
        familyService.displayAllFamilies();
    }


    public List<Family> getFamiliesLessThan (int number){
        return familyService.getFamiliesLessThan(number);
    }

    public List<Family> getFamiliesBiggerThan(int number){
        return familyService.getFamiliesBiggerThan(number);
    }


    public int countFamiliesWithMemberNumber (int number){
        return familyService.countFamiliesWithMemberNumber(number);
    }



    public void createNewFamily(Human mother, Human father){
        familyService.createNewFamily(mother,father);
    }


    public void deleteFamilyByIndex(int index){
        familyService.deleteFamilyByIndex(index);
    }


    public Family bornChild(Family family , String boyName , String girlName){
        return familyService.bornChild(family,boyName,girlName);
    }


    public Family adoptChild(Family family,Human child){
        return familyService.adoptChild(family,child);
    }


    public void deleteAllChildrenOlderThen(int age){
        familyService.deleteAllChildrenOlderThen(age);
    }


    public int count(){
        return familyService.count();
    }

    public Family getFamilyById(int index){
        return familyService.getFamilyById(index);
    }


    public Set<Pet> getPets(int index){
        return familyService.getPets(index);
    }

    public void addPet(int index,Pet pet){
            familyService.addPet(index,pet);
    }

    public FamilyService getFamilyService() {
        return familyService;
    }
}
