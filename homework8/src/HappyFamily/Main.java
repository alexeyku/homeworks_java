package HappyFamily;


public class Main {
    public static void main(String[] args) {
        String[] arr = {"eat, drink, sleep"};
        String[][] motherScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"walk with child"},
        };
        String[][] fatherScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"visit parents"}
        };

        String[][] childScedule = {
                {DayOfWeek.SATURDAY.name(),"relax with family"},
                {DayOfWeek.SUNDAY.name(),"walk with mother"}
        };



//        family 1

        Pet dog = new Pet(Species.DOG,"Bobik",7,51,arr);

        Human mother = new Human("Mary", "Winchester",1977,90,motherScedule);

        Human father = new Human("John", "Winchester",1974,95,fatherScedule);
        Family family = new Family(mother,father);
        Human child = new Human("Sam", "Winchester",2007,85 ,childScedule);

        Human child2 = new Human("Dean", "Winchester",2007,85 ,childScedule);


//        check duplication
        family.addChild(child);
        family.addChild(child2);
        family.addChild(child);
        family.addChild(child2);


//        System.out.println("count family:" + family.countFamily());
//        System.out.println("Deleting child...");
//        family.deleteChild(child);
//        System.out.println("count family:" + family.countFamily());
//
//        System.out.println("Without pet:");
//        System.out.println(child.greetPet());
//        family.setPet(dog);
//        System.out.println("With pet:");
//        System.out.println(child.greetPet());
//
//
//        System.out.println("Full family:");
//        System.out.println(family.toString());
//
//        System.out.println("Do these people belong to one family? :");
//        System.out.println(child.getFamily().getClass() == mother.getFamily().getClass() && child.getFamily().getClass() == father.getFamily().getClass());

        int counter = 0;
        while (counter < 500000){
            Human obj = new Human("Mary", "Winchester",1977,90,motherScedule);
            Human obj2 = new Human("Jane", "Winchester",1977,90,motherScedule);
            counter++;
        }
    }



}
