package HappyFamily;

import java.util.Arrays;
import java.util.Objects;

public class Pet {
    private Species species;
    private String nickname;
    private int age;
    private int trickLevel;
    private String[] habits;

    static {
        System.out.println("Pet initialised");
    }

    {
        System.out.println("New Pet`s object is creating");
    }

    public Pet(Species species, String nickname, int age, int trickLevel, String[] habits) {
        this.species = species;
        this.nickname = nickname;
        this.age = age;
        this.trickLevel = trickLevel;
        this.habits = habits;
    }

    protected void finalize () {
        System.out.println (nickname + " is deleting");
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Pet pet = (Pet) o;
        return  Objects.equals(species, pet.species) &&
                Objects.equals(nickname, pet.nickname);
    }

    @Override
    public int hashCode() {
        return species.hashCode() + nickname.hashCode() + age * 31;
    }

    //    methods
    public String eat() {
        return  "Я кушаю!";
    }

    public String respond() {
        return "Привет, хозяин. Я - " + nickname + ". Я соскучился!";
    }

    public String foul() {
        return "Нужно хорошо замести следы...";
    }

    public String toString() {
        String output = species.getDescription() + "{\n\tnickname = '" + nickname + "', age = " + age + ",trickLevel = " + trickLevel + ", can fly = " + species.isCanFly() + ", number of length = " + species.getNumberOfLegth() + ", has fur = " + species.isHasFur()  + ", habits = " + Arrays.toString(habits)+ "\n\t}";
        return output;
    }


    //    getters
    public Species getSpecies() {
        return species;
    }

    public String getNickname() {
        return nickname;
    }

    public int getAge() {
        return age;
    }

    public int getTrickLevel() {
        return trickLevel;
    }

    public String[] getHabits() {
        return habits;
    }

    public void setSpecies(Species species) {
        this.species = species;
    }

    public void setNickname(String nickname) {
        this.nickname = nickname;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public void setTrickLevel(int trickLevel) {
        this.trickLevel = trickLevel;
    }

    public void setHabits(String[] habits) {
        this.habits = habits;
    }
}
